package com.SingletonInWebD;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.wpl.qa.basePack.BaseClass;

public class DriverInit extends BaseClass{
	
	private static DriverInit instanceDriver =null;
	
	private DriverInit() {	
	}
	
	public WebDriver openBrowser() {
	
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
	}

	public static DriverInit getInstance() {
		if(instanceDriver == null)
			instanceDriver = new DriverInit();
			return instanceDriver;
	}
}
