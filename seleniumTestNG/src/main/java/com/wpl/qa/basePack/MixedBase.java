package com.wpl.qa.basePack;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

public class MixedBase {
	
	protected static WebDriver driver;
	public static Properties prop = new Properties();
	protected String browser;
	
	public MixedBase () {
	    try {
	    	FileInputStream fis = new FileInputStream("D:\\POmProject\\pomproject\\seleniumTestNG"
	    			+ "\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
	    	prop.load(fis);
	    }
	    catch (FileNotFoundException fnf) {
	    	fnf.printStackTrace();
	    }
         catch (IOException ex) {
	    	ex.printStackTrace();
	    }
	}
}
