package com.wpl.qa.basePack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.wpl.qa.util.TestUtils;

public class Browsers extends BaseClass{

	public static void chrome() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				  +"\\libraries\\chromeDriver\\chromedriver.exe"); 
		driver = new ChromeDriver();
		driver.manage().window().maximize(); //
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtils.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtils.IMPLICIT_WAIT, TimeUnit.SECONDS);
	}
	
	public static void firefox() {
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
				  +"\\libraries\\geckoDriver\\geckodriver.exe"); 
		driver = new FirefoxDriver();
		driver.manage().window().maximize(); //
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtils.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtils.IMPLICIT_WAIT, TimeUnit.SECONDS);
	}
	
}
