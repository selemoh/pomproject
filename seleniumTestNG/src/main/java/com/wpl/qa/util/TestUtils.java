package com.wpl.qa.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.wpl.qa.basePack.BaseClass;

public class TestUtils extends BaseClass{
	
	public static long PAGE_LOAD_TIMEOUT = 30;
	public static long IMPLICIT_WAIT = 30 ;
	public static String TesdDataFile = "D:\\NewRepoOne\\seleniumTestNG\\testData\\loginCredentials.xlsx";
	public static String confFilePath = "D:\\POmProject\\pomproject\\seleniumTestNG\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties";
		static Workbook book;
		static Sheet sheet;
		
	public static Object [][] gettestData(String sheetName) {
		FileInputStream file = null;
		
		try {
			file = new FileInputStream(TesdDataFile);
		} catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (EncryptedDocumentException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
			sheet = book.getSheet(sheetName);
			Object [][]data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for(int i=0; i<sheet.getLastRowNum(); i++) {
			for(int j=0; j<sheet.getRow(0).getLastCellNum(); j++) {
				data[i][j]=sheet.getRow(i+1).getCell(j).toString();
			}
		}
		return data;
	}
	
	
}
