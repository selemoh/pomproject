package com.wpl.qa.GeneralPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wpl.qa.EndUser.HomePage;
import com.wpl.qa.basePack.BaseClass;

public class WpLoginPage extends BaseClass {
	
	// PageFactory - Object Repository for WplLoginPage
	
		@FindBy(xpath="//input[@type='text']")
		WebElement U_id;
		
		@FindBy(xpath="//input[@type='password']")
		WebElement U_pass;
		
		@FindBy(xpath="//button[@type='button']")
		WebElement U_Signin;
		
		@FindBy(xpath="//div[@class='logo']//img[@class='gwt-Image']")
		WebElement wplLogo;
		
		// initializing all WebElements
		
		public WpLoginPage(){
			PageFactory.initElements(driver, this);
		}
		
		// Actions on LoginPage
		
		public String loginPageTitleValidation(){
			return driver.getTitle();
		}
		
		public boolean wplLogoValidation(){
			return wplLogo.isDisplayed();
		}
		
		public HomePage login(String U_name, String U_pwd){
			U_id.sendKeys(U_name);
			U_pass.sendKeys(U_pwd);
			U_Signin.click();
			return new HomePage();
		}

}
