package com.wpl.qa.EndUser;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.wpl.qa.basePack.BaseClass;

 public class HomePage extends BaseClass{
	
	@FindBy(xpath="//*[@id=\"header\"]/div/div[2]/div/div/div[2]/div[1]/div[2]/img") 
	@CacheLookup
	WebElement vehicleDropdown;
	
	@FindBy(xpath="//*[@id='header']/div/div[2]/div/div/div[2]/div[1]/div[3]/div/div[1]/div/a")
	@CacheLookup
	WebElement vehicleRegNum;
	
	@FindBy(xpath="//*[@id='middle']/div/div/div/div/div[1]/div[1]/div[1]/h3")
	@CacheLookup
	WebElement mavViewlable;
	
	@FindBy(xpath="//*[@id=\"middle\"]/div/div/div/div/div[2]/div[1]/div[2]/div/div[1]/div[2]/div")
	WebElement secureParkToggleBtn;
	
	@FindBy(xpath="//div[@class='whiteBox services']//div[2]//div[2]//div[2]")
	WebElement SpeedLimitBtn;
	
	@FindBy(xpath="(//a[contains(text(),'ON')])[2]")
	WebElement SpeedLimitToggleBtnOn;
	
	@FindBy(xpath= "/html/body/div[1]/div[2]/div/div/div/div/div[2]/div[1]/div[2]/div/div[2]/div[2]/div")
	WebElement SpeedLimitToggleBtnOff;
	
	@FindBy(xpath="/html/body/div[4]/div/div/div[2]/input")
	WebElement enterSpeedLimitValue;
	
	@FindBy(xpath="(//button[@type='button'])[12]")
	WebElement clickToSaveSpeedLimit;
	
// Initializing all WebElements through PageFactory	

	public HomePage(){
		PageFactory.initElements(driver, this);
	}
	
	
	// Page Actions
	
	public boolean checkSelectedVehicleRegNum(){
		vehicleDropdown.click();
		return vehicleRegNum.isDisplayed();
	}

	public String getSelectedVehicleRegNum(){
		vehicleDropdown.click();
		return vehicleRegNum.getText();
	}
	
	public boolean mapViewLableText(){
		return mavViewlable.isDisplayed();
	}
	
	public void scrollPage(){
		JavascriptExecutor jse = ((JavascriptExecutor) driver);
		jse.executeScript("window.scrollBy(0,380)","");
	}
	
	public void dscrollPage(){
		JavascriptExecutor jse = ((JavascriptExecutor) driver);
		jse.executeScript("window.scrollBy(0,180)","");
	}

	public boolean secureParkBtnAvailable(){
		return secureParkToggleBtn.isEnabled();
	}
	
	public void secureParkBtnFunction(){
		secureParkToggleBtn.click();
	}
	
	public boolean validateSpeedLimitBtn(){
		return SpeedLimitBtn.isDisplayed();
	}
	
	public void clickSpeedLimitBtn() throws InterruptedException{
		if(SpeedLimitToggleBtnOff.isDisplayed()) {
			SpeedLimitToggleBtnOff.click();
		}
		else if(SpeedLimitToggleBtnOn.isDisplayed()) {
			SpeedLimitToggleBtnOn.click();
		}
		     Thread.sleep(1000);
		    SpeedLimitToggleBtnOff.click();
		     Thread.sleep(3000);    
	}
	
	public boolean speedLimitTextField(){
		return enterSpeedLimitValue.isDisplayed();
	}
	
	public void speedLimitText(String speed) throws InterruptedException{
		Thread.sleep(2000);
		enterSpeedLimitValue.clear();
		enterSpeedLimitValue.sendKeys(speed);
	}
	
	public String getSpeedLimitText(){
		return enterSpeedLimitValue.getAttribute("value");
	}
	
	public void savingSpeedlimit() {
		clickToSaveSpeedLimit.click();
	}

}

