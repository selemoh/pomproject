package seleniumGrid;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class GridClass1 {

	WebDriver driver;
	
	@Test
	public void firstGridTest() throws InterruptedException, MalformedURLException{
		
		
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setBrowserName("chrome");
		caps.setPlatform(Platform.WINDOWS);
		
		ChromeOptions ops = new ChromeOptions();
		ops.merge(caps);
		
		driver = new RemoteWebDriver(new URL("http://192.168.1.204:4444/wd/hub"),ops);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.manage().window().maximize();
		
		driver.get("https://google.com");
		
		Thread.sleep(2000);
		
		driver.quit();
	}
}

// java -Dwebdriver.chrome.driver=path -jar selenium-standaloneserver-3.141.49.jar -port 5555 -browser browserName=chrome,maxInstances=2 -maxSession 2 -timeout 1200 -browserTimeout 10000 -cleanUpCycle 10000



