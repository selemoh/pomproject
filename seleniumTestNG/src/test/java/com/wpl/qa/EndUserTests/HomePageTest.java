package com.wpl.qa.EndUserTests;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.wpl.qa.EndUser.HomePage;
import com.wpl.qa.GeneralPages.WpLoginPage;
import com.wpl.qa.basePack.BaseClass;

public class HomePageTest extends BaseClass {

	WpLoginPage loginPage;
	HomePage EndUser_Home;

	public HomePageTest() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		// no comment
		initialize();
		loginPage = new WpLoginPage();
		loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		EndUser_Home = new HomePage();
	}

	@Test(priority = 1)
	public void vehicleRegNumTextTest() throws InterruptedException {
		Thread.sleep(5000);
		Assert.assertTrue(EndUser_Home.checkSelectedVehicleRegNum());
		System.out.println(" ");
		System.out.println("The Vehicle Reg Num is: "+EndUser_Home.getSelectedVehicleRegNum());
	}

	@Test(priority = 2)
	public void mapViewLableTest() throws InterruptedException {
		Thread.sleep(5000);
		Assert.assertTrue(EndUser_Home.mapViewLableText());
	}

	@Test(priority = 3)
	public void secureParkBtnVisibilityTest() throws InterruptedException {
		Thread.sleep(5000);
		EndUser_Home.scrollPage();
		Assert.assertTrue(EndUser_Home.secureParkBtnAvailable());
	}

	@Test(priority = 4, dependsOnMethods = "secureParkBtnVisibilityTest")
	public void secureParkBtnFnTest() throws InterruptedException {
		Thread.sleep(5000);
		EndUser_Home.scrollPage();
		EndUser_Home.secureParkBtnFunction();
	}

	@Test(priority = 5)
	public void SpeedLimitBtnVisibilityTest() throws InterruptedException {
		Thread.sleep(5000);
		EndUser_Home.scrollPage();
		Assert.assertTrue(EndUser_Home.validateSpeedLimitBtn(), "Speed limit toggle button is not present");
	}

	@Test(priority = 6)
	public void speedLimitFunctionTest() throws InterruptedException {
		Thread.sleep(12000);
		
		EndUser_Home.scrollPage();
		Thread.sleep(6000);
		
		EndUser_Home.clickSpeedLimitBtn();
		
		Assert.assertTrue(EndUser_Home.speedLimitTextField(), "Speed limit button is not clicked");
		
		EndUser_Home.speedLimitText(prop.getProperty("speedValue"));
		
		System.out.println(" ");
		System.out.println("This vehicle has a speed limit of:"+EndUser_Home.getSpeedLimitText()+" KMPH");
		
		EndUser_Home.savingSpeedlimit();
	}
	 
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if (ITestResult.SUCCESS == result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test case '" + result.getName() + "' is passed.");
			System.out.println(" ");
		}
		else if (ITestResult.FAILURE == result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test case '" + result.getName() + "' is failed.");
			System.out.println(" ");
		}
		Thread.sleep(2000);
		driver.quit();
	}
}
