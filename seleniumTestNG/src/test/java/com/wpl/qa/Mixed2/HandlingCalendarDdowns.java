package com.wpl.qa.Mixed2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingCalendarDdowns {
	
	public static void main(String []args) throws InterruptedException {
		
		WebDriver driver;
		System.setProperty("webdriver.chrome.driver", 
				System.getProperty("user.dir")+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
		// Initialize Webdriver object
		driver = new ChromeDriver();
		// Maximizing the browser window size
		driver.manage().window().maximize();
		// Deleting all the cookies before browsing
		driver.manage().deleteAllCookies();
		// Declaring the dynamic wait for page to be loaded
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		// Declaring additional dynamic wait for any other elements laoding at backend of the page
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Handling Basic authentication pop by providing username and password in the URL of the WebPage
		driver.get("");
		// Providing static wait to delay the session closing
		Thread.sleep(2000);
		// Closing the Webdriver session and browser
		driver.quit();
	}

}
