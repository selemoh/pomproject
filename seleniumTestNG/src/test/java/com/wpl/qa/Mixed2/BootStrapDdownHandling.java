package com.wpl.qa.Mixed2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BootStrapDdownHandling {
	
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", 
				System.getProperty("user.dir")+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Loading the Webpage by Passing the URL at browser
		driver.get("https://www.jquery-az.com/boots/demo.php?ex=63.0_2");
		//Clicking on Dropdown button at Webpage
		driver.findElement(By.xpath("//button[contains(@class,'multiselect')]")).click();
		
		// Now storing the text of all labels available on the drop-down list
		
		List<WebElement> list = driver.findElements(By.xpath("//ul[contains(@class,'multiselect-container')]//li//a//label"));
		System.out.println(" ");
		// Printing the Size of dropdown list
		System.out.println(list.size());
		System.out.println(" ");
		// To print the text of the labels we need a for loop and here we know the number of iterations also
		
//		for(int i=0; i<list.size(); i++) {
//			System.out.println(list.get(i).getText());
//			System.out.println(" ");
//		}
		
//		// Clicking on a particular label
//		for(int i=0; i<list.size(); i++) {			
//			if(list.get(i).getText().contains("Angular")) {
//				list.get(i).click();
//				break;
//			}
//		}
		
		// Clicking on all labels
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i).getText());
			System.out.println(" ");
			list.get(i).click();			
		}
		
		Thread.sleep(3000);
		// Closing the selenium session and browser
		driver.quit();
	}

}
