package com.wpl.qa.Mixed2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HandlingMiscAlerts {

	static WebDriver driver;
	
	static Properties prop = new Properties();
	
	static {
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
					+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
			prop.load(fis);
		}
		catch(FileNotFoundException fnf) {
			fnf.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeMethod
	public void initBrowser() {
		String browser = prop.getProperty("browser");
	if(browser.equals("chrome")) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	else if(browser.equals("firefox")) {
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
				+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
		driver = new FirefoxDriver();
	}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}
	
	@Test(priority=1)
	public void JavascriptAlerts() {
		driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
		WebDriverWait wait = new WebDriverWait(driver, 30);
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("proceed"))).click();
		
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		System.out.println("The text on alert pop up is: "+alertText);
		
		alert.accept();
		
		// If there is a cancel button then the followin command will be executed
		// alert.dismiss();
	}
	
	@Test(priority=2)
	public void fileUploadHandle() {
		driver.get("https://html.com/input-type-file/");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='pm-cta']"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='cn-accept-cookie']"))).click();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,700)", "");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='file' and @name='fileupload']")))
		.sendKeys(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
	}
	
	@Test(priority=3)
	public void newPopUpWindowHandling() throws InterruptedException {
		driver.get("http://www.popuptest.com/goodpopups.html");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		/*
		 * driver.switchTo().frame("");
		 * 
		 * Actions action = new Actions(driver);
		 * action.moveToElement(driver.findElement(By.xpath("")));
		 * driver.findElement(By.xpath("")).click();
		 * 
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * xpath("//input[@type='file' and @name='fileupload']")))
		 * .sendKeys(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
		 * 
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * xpath("//input[@type='file' and @name='fileupload']")))
		 * .sendKeys(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
		 * 
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * xpath("//input[@type='file' and @name='fileupload']"))).click();
		 * 
		 */
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='black']"))).click();
				 
		Set<String> handler = driver.getWindowHandles();
		Iterator<String> it = handler.iterator();
	
		String parentWinID = it.next();
		System.out.println("Parent window ID is: "+parentWinID);
		System.out.println("****************************************************");
		
		String childWinID = it.next();
		System.out.println("Child window ID is: "+childWinID);
		System.out.println("****************************************************");
		
		driver.switchTo().window(childWinID);
		String childWinTitle = driver.getTitle();
		System.out.println("The title of child window is: "+childWinTitle);
		System.out.println("*******************************************************");
		driver.close();
		
		driver.switchTo().window(parentWinID);
		String parentWinTitle = driver.getTitle();
		System.out.println("The title of child window is: "+parentWinTitle);
		System.out.println("*******************************************************");
	}
	
	@AfterMethod
	public void resultsNtearDown(ITestResult result) throws InterruptedException {
		if(ITestResult.SUCCESS==result.getStatus()) {
			System.out.println("Test case: "+result.getName()+" is PASSED");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		}
		else if(ITestResult.FAILURE==result.getStatus()) {
			System.out.println("Test case: "+result.getName()+" is FAILED");
			System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		}
		Thread.sleep(2000);
		driver.quit();
	}
}
