package com.wpl.qa.Mixed2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BootStrapDdownHandle2 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver;
		
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\libraries\\chromedriver_win32_N\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		// Loading the Webpage by Passing the URL at browser
		driver.get("https://getbootstrap.com/docs/4.3/components/dropdowns/");

		driver.findElement(By.id("dropdownMenuButton")).click();
		driver.findElement(By.id("dropdownMenuButton")).click();

		List<WebElement> list = driver
				.findElements(By.xpath("//div[@class='dropdown-menu'and @aria-labelledby='dropdownMenuButton']//a"));
		System.out.println(list.size());

		driver.findElement(By.id("dropdownMenuButton")).click();
		
		// Getting text of the menu items
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getText());
		}
		
		System.out.println(" ");
		// Clicking a particular label from dropdown menu
		for (int j = 0; j < list.size(); j++) {
			if (list.get(j).getText().contains("Another action")) {
				list.get(j).click();
				System.out.println("'Another action' option is clicked from the menu");
				break;
			}
		}
		Thread.sleep(2000);
		driver.quit();

	}
}
