package com.wpl.qa.test;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.wpl.qa.EndUser.HomePage;
import com.wpl.qa.GeneralPages.WpLoginPage;
import com.wpl.qa.basePack.BaseClass;
import com.wpl.qa.util.TestUtils;

public class WplLoginPageTest extends BaseClass {
	
	
// Object creation for the linked classes	
	WpLoginPage loginPage;
	HomePage EndUser_Home;
	String sheetName1 = "LoginData";
	String sheetName2 = "newLogin";
	TestUtils testUtils;
	
// Class constructor for initialization of BaseClass properties	
	public WplLoginPageTest(){
		super();
	}
	
// SetUp before the test execution	
	@BeforeMethod
	public void setUp(){
		log.info("Testing the message on console");
		initialize();
		loginPage = new WpLoginPage();
		testUtils = new TestUtils();
	}

	@DataProvider
	public Object[][] passData() {
		Object newData[][] =  TestUtils.gettestData(sheetName2);
		return newData;
	}
	
// Test Cases for the Login page
	
	@Test(priority=1)
	public void LoginPageTitleTest(){
		log.info("Test Title message");
		String pageTitle = loginPage.loginPageTitleValidation();
		Assert.assertEquals(pageTitle, "Waypals");
		System.out.println("\nCurrent page title is: "+driver.getTitle());
	}
	
	@Test(priority=2)
	public void LoginPageLogoTest(){
		Assert.assertTrue(loginPage.wplLogoValidation(), "The Logo is not available.");
	}

	@Test(priority=3, dataProvider="passData")
	public void End_UserLoginTest(String user, String pass) throws InterruptedException{
	//	EndUser_Home = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		loginPage.login(user, pass);
		Thread.sleep(2000);
		Assert.assertEquals(driver.getCurrentUrl(), prop.getProperty("euHomeUrl"), "User is not logged-in.");
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException{ 
		if(ITestResult.SUCCESS==result.getStatus()){
			System.out.println("Test Case '"+result.getName()+"' is passed");
			System.out.println(" ");
		}
		else if(ITestResult.FAILURE==result.getStatus()){
			System.out.println("Test Case '"+result.getName()+"' is failed");
			System.out.println(" ");
		}
		
		Thread.sleep(2000);
		driver.quit();
	}
	
}
