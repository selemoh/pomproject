package com.wpl.qa.test;

public class AutomatedTestUpdate {
	/*
	 * WebDriver driver; public static String DEVKEY =
	 * "35d6ed46ac1334792ef8521fb0a2c4f8"; public static String URL =
	 * "http://localhost:81/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
	 * 
	 * public void reportResult(String TestProject, String TestPlan, String
	 * TestCase, String Build, String Notes, String Result) throws
	 * TestLinkAPIException{ TestLinkAPIClient api = new TestLinkAPIClient(URL,
	 * DEVKEY); api.reportTestCaseResult(TestProject, TestPlan, TestCase, Build,
	 * Notes, Result); }
	 * 
	 * @Test(priority=1) public void Test1() throws Exception { AutomatedTestUpdate
	 * testing = new AutomatedTestUpdate();
	 * System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
	 * +"\\libraries\\chromeDriver_win32\\chromedriver.exe"); driver = new
	 * ChromeDriver(); String TestProject = "EndToEnd_Tests"; String TestPlan =
	 * "Waypals_Web_Test"; String TestCase = "LP_1"; String Build =
	 * "WPL_LoginTest_Build"; String Notes = null; String Result = null;
	 * 
	 * try{ driver.manage().window().maximize(); driver.manage().deleteAllCookies();
	 * driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	 * WebDriverWait wait = new WebDriverWait(driver, 30);
	 * driver.get("https://waypals.com/index.html");
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//a[contains(text(),'Login')]"))).click(); Thread.sleep(2000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * xpath("(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::label[1]"
	 * ))).click(); Thread.sleep(3000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//div[@id='wraper']/div[3]/ul/li[3]/div/label"))).click();
	 * Thread.sleep(2000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//div[@id='wraper']/div[3]/ul/li[4]/div/label"))).click();
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//a[contains(text(),'CONTINUE')]"))).click(); Thread.sleep(1000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//div[@id='wraper']/div[3]/ul/li[3]/div/label"))).click(); Result =
	 * TestLinkAPIResults.TEST_PASSED; Notes = TestLinkAPIResults.API_RESULT_NOTES;
	 * 
	 * } catch (Exception e) { Result = TestLinkAPIResults.TEST_FAILED; Notes =
	 * "Execution failed"; e.printStackTrace(); }
	 * 
	 * finally{ testing.reportResult(TestProject, TestPlan, TestCase, Build, Notes,
	 * Result); Thread.sleep(2000); driver.quit(); }
	 * 
	 * }
	 * 
	 * @Test(priority=2) public void Test2() throws Exception { AutomatedTestUpdate
	 * testing = new AutomatedTestUpdate();
	 * System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
	 * +"\\libraries\\chromeDriver_win32\\chromedriver.exe"); driver = new
	 * ChromeDriver(); String TestProject = "EndToEnd_Tests"; String TestPlan =
	 * "Waypals_Web_Test"; String TestCase = "LP_1.1"; String Build =
	 * "WPL_LoginTest_Build"; String Notes = null; String Result = null;
	 * 
	 * try{ driver.manage().window().maximize(); driver.manage().deleteAllCookies();
	 * driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	 * WebDriverWait wait = new WebDriverWait(driver, 30);
	 * driver.get("https://waypals.com/wpl/#LOGIN");
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//input[@type='text']"))).sendKeys("mohit@waypals.com");
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//input[@type='password']"))).sendKeys("ajwaypals");
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//button[@type='button']"))).click(); Thread.sleep(1000);
	 * System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(
	 * By.xpath("//div[2]/div/div/div/img"))).isDisplayed()); Thread.sleep(1000);
	 * Result = TestLinkAPIResults.TEST_PASSED; Notes = "Executed Successfully"; }
	 * catch (Exception e) { Result = TestLinkAPIResults.TEST_FAILED; Notes =
	 * "Execution failed"; e.printStackTrace(); }
	 * 
	 * finally{ testing.reportResult(TestProject, TestPlan, TestCase, Build, Notes,
	 * Result); Thread.sleep(2000); driver.quit(); }
	 * 
	 * }
	 * 
	 * @Test(priority=3) public void Test3() throws Exception { AutomatedTestUpdate
	 * testing = new AutomatedTestUpdate();
	 * System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
	 * +"\\libraries\\chromeDriver_win32\\chromedriver.exe"); driver = new
	 * ChromeDriver(); String TestProject = "EndToEnd_Tests"; String TestPlan =
	 * "Waypals_Web_Test"; String TestCase = "LP_1.2"; String Build =
	 * "WPL_LoginTest_Build"; String Notes = null; String Result = null;
	 * 
	 * try{ driver.manage().window().maximize(); driver.manage().deleteAllCookies();
	 * driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	 * WebDriverWait wait = new WebDriverWait(driver, 30);
	 * driver.get("https://waypals.com/index.html");
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//a[contains(text(),'Login')]"))).click(); Thread.sleep(2000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
	 * xpath("(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::label[1]"
	 * ))).click(); Thread.sleep(3000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//div[@id='wraper']/div[3]/ul/li[3]/div/label"))).click();
	 * Thread.sleep(2000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//div[@id='wraper']/div[3]/ul/li[4]/div/label"))).click();
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//a[contains(text(),'CONTINUE')]"))).click(); Thread.sleep(1000);
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * "//div[@id='wraper']/div[3]/ul/li[3]/div/label"))).click(); Result =
	 * TestLinkAPIResults.TEST_PASSED; Notes = "Executed Successfully";
	 * 
	 * } catch (Exception e) { Result = TestLinkAPIResults.TEST_FAILED; Notes =
	 * "Execution failed"; e.printStackTrace(); }
	 * 
	 * finally{ testing.reportResult(TestProject, TestPlan, TestCase, Build, Notes,
	 * Result); Thread.sleep(2000); driver.quit(); }
	 * 
	 * }
	 */
}
