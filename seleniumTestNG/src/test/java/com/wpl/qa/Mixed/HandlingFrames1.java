package com.wpl.qa.Mixed;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.wpl.qa.basePack.MixedBase;

public class HandlingFrames1 extends MixedBase{

	public HandlingFrames1() {
		super();
	}
	
	@BeforeMethod
	public void launchingBrowser() {
		
	browser = prop.getProperty("browser");
	if(browser.equals("chrome")) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	else if(browser.equals("firefox")) {
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
				+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
		driver = new FirefoxDriver();
	}	
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(35, TimeUnit.SECONDS);
	}
	
	@Test
	public void handleFrames() throws InterruptedException{
		driver.get("https://www.crmpro.com/index.html");
		WebDriverWait wait  = new WebDriverWait(driver, 30);
		Thread.sleep(8000);
		driver.switchTo().frame("intercom-borderless-frame");
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//div[contains(@class,'intercom-author-summary-name-from')]"))).build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath
				("//div[contains(@class,'intercom-borderless-dismiss-button')]"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath
				("//input[@type='text' and @name='username']"))).sendKeys(prop.getProperty("crmuser"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath
				("//input[@type='password' and @name='password']"))).sendKeys(prop.getProperty("crmUpass"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath
				("//input[@type='submit' and @class='btn btn-small']"))).click();
		Thread.sleep(5000);
		// Switching to the Frame
		driver.switchTo().frame("mainpanel");
		//Clicking on Contacts page link
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath
				("//a[contains(text(), 'Contacts')]"))).click();
		System.out.println(" ");
		System.out.println("Handled frame and clicked on the Contacts page link successfully");
	}
	
	@AfterMethod
	public void tearDownNresults(ITestResult result) throws InterruptedException{
		if (ITestResult.SUCCESS==result.getStatus()) {
			System.out.println(" ");
			System.out.println("The Test case: "+result.getName()+" is Passed");
			System.out.println(" ");
		}
		
		else if (ITestResult.FAILURE==result.getStatus()) {
			System.out.println(" ");
			System.out.println("The Test case: "+result.getName()+" is Failed");
			System.out.println(" ");
		}
		Thread.sleep(2000);
		driver.quit();
	}
}
