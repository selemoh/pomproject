package com.wpl.qa.Mixed;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HandleMiscPopUps {
	
	WebDriver driver;
	Alert alert;
	
	@BeforeMethod
	public void initBrowser() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

	}
	
	@Test(priority=1)
	public void HandleAlertPopUps() throws InterruptedException{		
		driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("proceed"))).click();
		// referncing the alert class object to get the alert values
	    alert = driver.switchTo().alert();
		System.out.println("The text present on alert pop is: "+alert.getText());
	    // Validating the alert text by using Hard assertion
	    Assert.assertEquals(alert.getText(), "Please enter a valid user name" , "Alert text is not matching");
		// Clicking on OK button
		alert.accept();
	    //Clicking on cancel button
	//	alert.dismiss();
		
	}
	
	@Test(priority=2)
	public void HandlefileUploadPopUp() throws InterruptedException{	
		driver.get("https://html.com/input-type-file/");
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cn-accept-cookie\"]"))).click();
		// //*[@id="cn-accept-cookie"]
		Thread.sleep(2000);
		JavascriptExecutor jse = ((JavascriptExecutor)driver);
		jse.executeScript("window.scrollBy(0,950)","");
		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fileupload\"]"))).sendKeys(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
		Thread.sleep(1000);
	}
	
	@Test(priority=3)
	public void HandlePopUpWindow() throws InterruptedException{
		Thread.sleep(1000);
		driver.get("http://www.popuptest.com/goodpopups.html");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/table[2]/tbody/tr/td/font/b/a[3]"))).click();
		Thread.sleep(1000);
		
		Set<String> handler = driver.getWindowHandles();
	
		Iterator<String> it = handler.iterator();
		
		String parentWindowId = it.next();
		System.out.println("The Parent window id is: "+ parentWindowId);
		
		String childWindowId = it.next();
		System.out.println("The child window id is: "+ childWindowId);
		
		driver.switchTo().window(childWindowId);		
		String ChildwindowTitle = driver.getTitle();
		System.out.println("The title of child window is: "+ChildwindowTitle);
		
		driver.close();
		
		driver.switchTo().window(parentWindowId);
		String ParentwindowTitle = driver.getTitle();
		System.out.println("The title of child window is: "+ParentwindowTitle);
		
		Thread.sleep(1000);
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if(ITestResult.SUCCESS==result.getStatus()) {
			System.out.println("The Test case - "+result.getName()+" is Passed");
		}
		else if(ITestResult.FAILURE==result.getStatus()) {
			System.out.println("The Test case - "+result.getName()+" is Failed");
		}
		else if(ITestResult.SKIP==result.getStatus()) {
			System.out.println("The Test case - "+result.getName()+" is Skipped");
		}
		
		Thread.sleep(2000);
		driver.quit();
	}
}
