package com.wpl.qa.Mixed;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.wpl.qa.basePack.MixedBase;

public class DynamicWebTableHandling extends MixedBase {

	public DynamicWebTableHandling() {
		super();
	}

	@BeforeMethod
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	}

	@Test
	public void DynamicWebTable() throws InterruptedException {
		driver.get("https://www.crmpro.com/index.html");
		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='text']")))
				.sendKeys(prop.getProperty("crmuser"));

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password']")))
				.sendKeys(prop.getProperty("crmUpass"));

		Thread.sleep(8000);
		driver.switchTo().frame("intercom-borderless-frame");

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath(
				"//div[contains(@class,'intercom-chat-card intercom-chat-card-borderless intercom-chat-card-with-avatar')]")))
				.build().perform();

		driver.findElement(By.xpath("//div[contains(@class,'intercom-borderless-dismiss-button')]")).click();

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit' and @value='Login']")))
				.click();

		Thread.sleep(6000);

		driver.switchTo().frame("mainpanel");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Contacts')]"))).click();

		// To select a check box in dynamic web table we can two diffrent methods -

		// Method 1

		/*
		 * String beforeXpath = "//form[@id='vContactsForm']/table/tbody/tr["; String
		 * afterXpath = "]/td[2]";
		 * 
		 * for (int i = 4; i <= 7; i++) {
		 * 
		 * String name = driver.findElement(By.xpath(beforeXpath + i +
		 * afterXpath)).getText(); System.out.println(name);
		 * 
		 * if (name.contains("Narendra Singh")) {
		 * driver.findElement(By.xpath("//form[@id='vContactsForm']/table/tbody/tr[" + i
		 * + "]/td/input")).click(); }
		 * 
		 * Thread.sleep(4000);
		 * 
		 * }
		 */
		
		// Method 2:
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(type(),'')]/parent::td//preceding-sibling::td//a[contains(type(),'')]"))).click();
		
		driver.quit();
	}

}
