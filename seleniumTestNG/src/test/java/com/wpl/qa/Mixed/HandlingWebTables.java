package com.wpl.qa.Mixed;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HandlingWebTables {

	static WebDriver driver;
	
	static Properties prop  = new Properties();
	
	static {
		try {
		FileInputStream fis = new FileInputStream("D:\\POMProject\\pomproject\\seleniumTestNG"
				+ "\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
		prop.load(fis);
	} 
	catch (FileNotFoundException nF) {
		nF.printStackTrace();
	}
	catch (IOException e) {
		e.printStackTrace();
	}
	}
	
	@BeforeMethod
	public void launchAUT(){
		String browser = prop.getProperty("browser"); 
		if(browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if (browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void DynamicWebtable() throws InterruptedException {
		
		driver.get("https://www.crmpro.com/index.html");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//input[@type='text']"))).sendKeys(prop.getProperty("crmuser"));
		wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//input[@type='password']"))).sendKeys(prop.getProperty("crmUpass"));
		Thread.sleep(8000);
		driver.switchTo().frame("intercom-borderless-frame");
		Actions action  = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//div[contains(@class,'intercom-chat-card intercom-chat-card-borderless intercom-chat-card-with-avatar')]"))).build().perform();
		driver.findElement(By.xpath("//div[contains(@class,'intercom-borderless-dismiss-button')]")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated
				(By.xpath("//input[@type='submit' and @value='Login']"))).click();
		
		Thread.sleep(5000);
		
		driver.switchTo().frame("mainpanel");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Contacts')]"))).click();
			
		// Selecting the contacts by clicking on Checkbox
		
		// Method 1:
		
		Thread.sleep(3000);
	
//		String BeforeXpath = "//form[@id='vContactsForm']/table/tbody/tr[";
//		String AfterXpath = "]/td[2]";
		
		
		//  //form[@id='vContactsForm']/table/tbody/tr[7]/td/input
		
		/*
		 * for (int i=4; i<=8; i++) { String Names =
		 * driver.findElement(By.xpath(BeforeXpath+i+AfterXpath)).getText();
		 * System.out.println(Names); if(Names.contains("Narendra Singh")) {
		 * driver.findElement(By.xpath("//form[@id='vContactsForm']/table/tbody/tr["+i+
		 * "]/td/input")).click(); } }
		 */
		
	//	Method 2
		
		// Method 2 is fast as because of no for loop and customized 
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Surya Tyagi')]/parent::td//preceding-sibling::td//input[@type='checkbox']"))).click();	
		
		Thread.sleep(3000);
		  driver.quit();
	}
}
