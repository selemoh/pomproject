package com.wpl.qa.Mixed;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HandleMiscPopUps3 {
	
	WebDriver driver;
	static Properties prop = new Properties();
	
	static {	
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
					+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
			prop.load(fis);
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException t) {
			t.printStackTrace();
		}
	}
	
	@BeforeMethod
	public void launchBrowser() {
		String Browse = prop.getProperty("browser");
		if(Browse.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else if(Browse.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);	
	}
	
	@Test(priority=1)
	public void popUpAlert() throws InterruptedException{
		driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("proceed"))).click();
		Alert alert = driver.switchTo().alert();
		System.out.println("The text on alert message is: "+alert.getText());
		alert.accept();
//		alert.dismiss();
		}
	
	@Test(priority=2)
	public void HandleFileUpload() throws InterruptedException{
		driver.get("https://html.com/input-type-file/");
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"pushmonkey-prompt-container\"]/div/div[3]/a[1]"))).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cn-accept-cookie"))).click();
		
		JavascriptExecutor jse = ((JavascriptExecutor)driver);
		jse.executeScript("window.scrollBy(0, 700)", "");
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fileupload\"]")))
		.sendKeys(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
	}
	
	@Test(priority=3) 
	public void HandleWindowSwitch() throws InterruptedException{
		driver.get("http://www.popuptest.com/goodpopups.html");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/table[2]/tbody/tr/td/font/b/a[3]"))).click();
		
		Set<String> handler = driver.getWindowHandles();
		
		Iterator<String> it = handler.iterator();
		
		String parendWindowID = it.next();
		System.out.println(parendWindowID);
		
		String childWindowID = it.next();
		System.out.println(childWindowID);
		
		driver.switchTo().window(childWindowID);
		System.out.println("The title of the child window is: "+driver.getTitle());
		
		driver.close();
		
		driver.switchTo().window(parendWindowID);
		System.out.println("The title of the Parent window is: "+driver.getTitle());
	}

	
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if(ITestResult.SUCCESS==result.getStatus()) {
			System.out.println("The test case: "+result.getName()+" is Passed");
		}
		else if(ITestResult.FAILURE==result.getStatus()) {
			System.out.println("The test case: "+result.getName()+" is Failed");
		}
		
		else if(ITestResult.SKIP==result.getStatus()) {
			System.out.println("The test case: "+result.getName()+" is Skipped");
		}
		Thread.sleep(2000);
		driver.quit();
	}
	
	

}
