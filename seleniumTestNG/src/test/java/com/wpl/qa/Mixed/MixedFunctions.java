package com.wpl.qa.Mixed;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MixedFunctions {
	// Declaring WebDriver class object.
	WebDriver driver;
	// Declaring and initializing Properties class 
	Properties prop = new Properties();
	// Declaring Alert class object
	
//	// Setting up the initBrowser method to launch browser
//	@BeforeMethod
//	// Passing the parameters fro testng.xml file with the help of Parameters annotation
////	@Parameters({"browser","redurl"})
////	public void initBrowser(String browser, String redurl) {
//		
//	public void initBrowser() {	
//		  // Declaring FileInputStream class to locate the connfiguration file surrounded with try-multi catch block 
//		  try { 
//			  FileInputStream fis = new  FileInputStream(System.getProperty("user.dir") +
//		  "\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties"); 
//		  // Invoking the properties file through load 
//		  prop.load(fis); 
//		  } 
//		  // Thrown FileNotFoundException to handle the file not found error/exception
//		   catch(FileNotFoundException e) 
//		  { e.printStackTrace(); 
//		  } 
//		  // Thrown IOException to handle input and output exception
//		   catch(IOException t) 
//		  { t.printStackTrace();
//		  }
//		 	
//		
//		// Setting If else-if condition to get the browser key from properties file.
//		
//		// Declaring String reference for browser name property.
//		String browser1 = prop.getProperty("browser");		
//		if(browser1.equals("chrome")) {
//			// Setting up the key values by System.setProperty to select the browser types and browser driver executables
//			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
//			+"\\libraries\\chromedriver_win32\\chromedriver.exe");
//			// Initializing driver object reference class with ChromeDriver 
//			driver = new ChromeDriver();
//		}
//		else if(browser1.equals("firefox")) {
//			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
//					+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
//			// Initializing driver object reference class with FirefoxDriver
//			driver = new FirefoxDriver();
//		}
//		// Maximizing browser window
//		driver.manage().window().maximize();
//		// Manage to delete all browser cookies
//		driver.manage().deleteAllCookies();
//		// Managing page load time
//		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//		// Getting and Passing the AUT URL 
//	//	driver.get(redurl);
//		driver.get(prop.getProperty("redurl"));
//	}
	
	@BeforeMethod
	public void myBrowser() {
	//	 Declaring FileInputStream class to locate the connfiguration file surrounded with try-multi catch block 
		  try { 
			  FileInputStream fis = new  FileInputStream(System.getProperty("user.dir") +
		  "\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties"); 
		  // Invoking the properties file through load 
		  prop.load(fis); 
		  } 
		  // Thrown FileNotFoundException to handle the file not found error/exception
		   catch(FileNotFoundException e) 
		  { e.printStackTrace(); 
		  } 
		  // Thrown IOException to handle input and output exception
		   catch(IOException t) 
		  { t.printStackTrace();
		  }
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32\\chromedriver.exe");
//		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
//				+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
		
		driver = new ChromeDriver();
//		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(35, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("redurl"));
	
	}
	
	@Test
	public void HandlingAlertNPopups() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 30);
	// Locating and clicking on Submit 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div/div[1]/div[2]/div[2]/div[1]/div/form/div/div[6]/div[1]/input"))).click();
		Thread.sleep(3000);
	// Initializing object class reference with driver.switchTo() method to handle web page alert
	   Alert alert = driver.switchTo().alert(); 	
		Thread.sleep(2000);
		System.out.println("The text on alert message is: ''"+ alert.getText() +"''");
		//Printing the text on alert pop up
	Assert.assertEquals(alert.getText(), "Please enter a valid user name", "Alert text is not matching: ");

	System.out.println(" ");
//		System.out.println("The text on alert message is: ''"+ alertText +"''");
	// To click on OK button of the alert we use
	alert.accept();
	//	To click on Cancel button we use
	//	alert.dismiss();
	
	}
	
	
	@AfterMethod
	// Parameterizing ITestResult interface reference of TestNG to print the status of the test.
	public void tearDown(ITestResult result) throws InterruptedException{
		//Setting up if else-if ladder block to get the either kind of result pass or fail
		if(ITestResult.SUCCESS==result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test Case:'"+result.getName()+"' is Passed.");
			System.out.println(" ");
		}
		else if(ITestResult.FAILURE==result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test Case:'"+result.getName()+"' is Failed.");
			System.out.println(" ");
		}
		
		else if(ITestResult.SKIP==result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test Case:'"+result.getName()+"' is Skipped.");
			System.out.println(" ");
		}
	
		Thread.sleep(3000);
		
		// Closing the browser and WebDriver session
		driver.quit();
	}
	
	
	
	

}
