package com.wpl.qa.Mixed;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HandleMiscPopups2 {
	
	WebDriver driver;
	
	@BeforeMethod
	public void testSetup() throws FileNotFoundException{
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
				+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
		try {
			prop.load(fis);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		String browser1 = prop.getProperty("browser") ;
		if (browser1.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			System.out.println("Launching the browser");
		}
		
		else if (browser1.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
			driver = new FirefoxDriver();
			System.out.println("Launching the browser");
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		
		
	}
	
	@Test(priority=1)
	public void handleAlerts() throws InterruptedException {
		driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
		WebDriverWait wait  = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("proceed"))).click();
		Thread.sleep(2000);
		Alert alert = driver.switchTo().alert();
		System.out.println("The text on the alert is: "+alert.getText());
		alert.accept();
	// To Reject the alert
		// alaert.dismiss();
	}
	
	@Test(priority=2)
	public void handleFileUpload() throws InterruptedException {
		driver.get("https://html.com/input-type-file/"); 
		WebDriverWait wait  = new WebDriverWait(driver, 30);		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"pushmonkey-prompt-container\"]/div/div[3]/a[1]"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cn-accept-cookie"))).click();
		JavascriptExecutor jse = ((JavascriptExecutor)driver);
		jse.executeScript("window.scrollBy(0,780)", "");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"fileupload\"]"))).sendKeys
		(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
		Thread.sleep(1000);
	}
	
	@Test(priority=3)
	public void handleWindowSwitchPopUp() {
		driver.get("http://www.popuptest.com/goodpopups.html"); 
		WebDriverWait wait  = new WebDriverWait(driver, 30);		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/table[2]/tbody/tr/td/font/b/a[3]"))).click();
		
		Set<String> handler =  driver.getWindowHandles();
		Iterator<String> it = handler.iterator();
		
		String parentwindowId = it.next();
		System.out.println("The Id of parent Window is: "+ parentwindowId);
		
		String childwindowId = it.next();
		System.out.println("The Id of child Window is: "+ childwindowId);
		
		driver.switchTo().window(childwindowId);
		String childWindowTitle = driver.getTitle();
		System.out.println("The title of child window is: " +childWindowTitle);
		
		driver.close();
		
		driver.switchTo().window(parentwindowId);
		String parentWindowtitle = driver.getTitle();
		System.out.println("The title of parent window is: "+parentWindowtitle);
		
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if(ITestResult.SUCCESS==result.getStatus()) {
			System.out.println("Test case status: "+result.getName()+" is Passed");
		}
		
		else if(ITestResult.FAILURE==result.getStatus()) {
			System.out.println("Test case status: "+result.getName()+" is Failed");
		}
		
		else if(ITestResult.SKIP==result.getStatus()) {
			System.out.println("Test case status: "+result.getName()+" is Skipped");
		}
		Thread.sleep(2000);
		System.out.println(" ");
		System.out.println("Closing the browser and WebDriver test.");
		driver.quit();
	}



}
