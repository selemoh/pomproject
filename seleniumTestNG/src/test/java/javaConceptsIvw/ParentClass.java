package javaConceptsIvw;

public class ParentClass {

	public final void start() {
		System.out.println("This is parent class");
	}
	
	public final void end() {
		System.out.println("Test method");
	}

}
