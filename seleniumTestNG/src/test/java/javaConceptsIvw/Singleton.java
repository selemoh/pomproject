package javaConceptsIvw;

public class Singleton {

	// Singleton class is a class that can have only and only one object at a time.\
			// Rules to design Singleton class:
			// 1. Make a private constructor of the class
			// 2. Write a public static method (getInstance) that has return type of object of this singleton class - It is also 
			// called Lazy Initialization
			// We need to create a private static variable also.
			// Difference between Normal class and Singleton class
			// In normal class we use constructor whereas in Singleton class we use getInstance method for instantiation. 
		
	private static Singleton singleton_instance = null;
	public String str; // This is optional
	
	private Singleton() {
		str = "I am learning Singleton concept";
	}
	
	public static Singleton getInstance() {	
		if(null == singleton_instance) {
			singleton_instance = new Singleton();
		}
		return singleton_instance;
	}
	public static void main(String[] args) {
	// Creating an object of a class
		Singleton a = Singleton.getInstance();
		Singleton b = Singleton.getInstance();
		Singleton c = Singleton.getInstance();
		
		a.str = (a.str).toUpperCase();
		
		System.out.println(a.str);
		System.out.println(b.str);
		System.out.println(c.str);
// Same values should be printed for the other objects as in Singleton concept only one object can be created and more than one objects
// will point to the same object
		
		b.str = (b.str).toLowerCase();
		System.out.println(" ");
		System.out.println(a.str);
		System.out.println(b.str);
		System.out.println(c.str);
	
	}
}
