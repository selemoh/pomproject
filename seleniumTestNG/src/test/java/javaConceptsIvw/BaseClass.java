package javaConceptsIvw;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.wpl.qa.util.TestUtils;

public class BaseClass {
	public static final Logger log = Logger.getLogger(BaseClass.class.getName());
	public static WebDriver driver;
	public static Properties prop;
	String log4jConfPath = "log4j.properties";
//	public static Logger log = Logger.getLogger("devpinoyLogger");

	
	  public BaseClass() {
	  
	  try { prop = new Properties(); FileInputStream fis = new FileInputStream(
	  "D:\\NewRepoOne\\seleniumTestNG\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties"
	  ); prop.load(fis); } catch (FileNotFoundException e) { e.printStackTrace(); }
	  catch (IOException t){
	  
	  t.printStackTrace(); } }
	  
	  public void initialize(){ // log.info("Launching the Browser"); 
		  String browser = prop.getProperty("browser"); if (browser.equals("chrome")){
	  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
	  +"\\libraries\\chromedriver_win32\\chromedriver.exe"); driver = new
	  ChromeDriver(); } else if (browser.equals("firefox")){
	  System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
	  +"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe"); driver = new
	  FirefoxDriver(); } driver.manage().window().maximize(); //
	  driver.manage().deleteAllCookies();
	  driver.manage().timeouts().pageLoadTimeout(TestUtils.PAGE_LOAD_TIMEOUT,
	  TimeUnit.SECONDS);
	  driver.manage().timeouts().implicitlyWait(TestUtils.IMPLICIT_WAIT,
	  TimeUnit.SECONDS);
	  
	  driver.get(prop.getProperty("testurl")); }
	  
	/*
	 * @BeforeTest
	 * 
	 * @Parameters({"browser", "url"}) public void setUp(String browser, String
	 * url){ if (browser.equals("chrome")){
	 * System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
	 * +"\\libraries\\chromedriver_win32\\chromedriver.exe"); driver = new
	 * ChromeDriver(); } else if (browser.equals("firefox")){
	 * System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
	 * +"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe"); driver = new
	 * FirefoxDriver(); } driver.manage().window().maximize();
	 * driver.manage().deleteAllCookies();
	 * driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
	 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 * driver.get(url); }
	 */

}
