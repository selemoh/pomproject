package javaConceptsIvw;

public class FinalizeConcept {

	public void finalize() {
		System.out.println("Inside finalize method");
	}
// finalize method is used for the cleanup process before the garbage collection	
	public static void main(String[] args) {

		@SuppressWarnings("unused")
		FinalizeConcept f1 = new FinalizeConcept();
		@SuppressWarnings("unused")
		FinalizeConcept f2 = new FinalizeConcept();
		f1 = null;
		f2 = null;
		System.gc();
	}

}
