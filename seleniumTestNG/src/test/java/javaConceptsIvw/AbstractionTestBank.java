package javaConceptsIvw;

public class AbstractionTestBank {

	public static void main(String[] args) {

		Icici ib= new Icici();
		ib.loan();
		ib.credit();
		ib.debit();
		ib.funds();
		
		// Parent class object refernce cannot inherit the child class method
		
		Bank nb = new Icici();
		nb.loan();
		nb.credit();
		nb.debit();
	}

}
