package javaConceptsIvw;

public class AbstactionItestCar {

	public static void main(String[] args) {

		MercedezCar mcar = new MercedezCar();
		mcar.start();
		mcar.stop();
		mcar.refuel();
		mcar.theftSafety();
		
		Car ncar = new MercedezCar();
		ncar.start();
		ncar.stop();
		ncar.refuel();
		
	}

}
