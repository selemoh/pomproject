package javaConceptsIvw;

public class FinallyConcept {

	public static void main(String[] args) {
//		oneMethod();
//		twoMethod();
		threeMethod();
	}
// script inside the finally blocked will be executed even if there is an exception.	
	public static void oneMethod() {
		@SuppressWarnings("unused")
		int i = 10;
		try {
			System.out.println("Inside try block of oneMethod");
		}
		catch(Exception e) {
			System.out.println("Inside catch block of oneMethod");
		}
		finally {
			System.out.println("inside finally block of oneMethod");
		}
	}

	public static void twoMethod() {
		int i = 10;
		try {
			System.out.println("Inside try block of twoMethod");
			i = i/0;
		}
		catch(ArithmeticException e) {
			System.out.println("Inside catch block of twoMethod");
		}
		finally {
			System.out.println("inside finally block of twoMethod");
		}
	}

	public static void threeMethod() {
		int i = 10;
		try {
			System.out.println("Inside try block of threeMethod");
			i = i/0;
		}
		catch(NullPointerException e) {
			System.out.println("Inside catch block of threeMethod");
		}
		finally {
			System.out.println("inside finally block of threeMethod");
		}
	}
}
