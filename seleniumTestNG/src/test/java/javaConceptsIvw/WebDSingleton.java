package javaConceptsIvw;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.SingletonInWebD.DriverInit;
import com.wpl.qa.basePack.BaseClass;

public class WebDSingleton extends BaseClass {

	@BeforeMethod
	public void launchBrowser() {
		DriverInit instanceDriver = DriverInit.getInstance();
		instanceDriver.openBrowser();
	}
	
	@Test
	public void titleTest() throws InterruptedException {
		driver.get("https://www.google.com/");
		Thread.sleep(10000);
		System.out.println(driver.getTitle());
	}
	
	@AfterMethod
	public void tearDown() {
		System.gc();
		driver.quit();
	}
	
	}
