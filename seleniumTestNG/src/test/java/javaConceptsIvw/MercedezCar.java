package javaConceptsIvw;

public class MercedezCar implements Car {

	@Override
	public void start() {
		System.out.println("\nCar is started");
	}

	@Override
	public void stop() {
		System.out.println("Car is stopped now");
	}

	@Override
	public void refuel() {
		System.out.println("Car needs to be refueled");
	}
	
	// Non-overidden method
	
	public void theftSafety() {
		System.out.println("This car has advanced theft safety feature inbuilt");
	}

}
