package javaConceptsIvw;

public class FinalConcept {
	
	public static void main(String[] args) {

		// final - final keyword is used to define the constant value
		// value of variable declared with final keyword cannot be changed
			// final - final keyword is used to prevent inheritance
			// final - final keyword is used to prevent method overriding
			
			// finally is a code block having starting and Ending body.
			// A finally block is present after the catch block of try-catch
			// finally keyword is always used with try-catch block
			// The script inside finally keyword will be executed even an exception is thrown.
			// finally bloc will always be executed
			
			// Finalize is used to perform the cleanup memory processing
			// finalize method is called before garbage collector just to clean process of the object
			// finalize is used for cleanup process before the garbage collector is called.
			
//			finally{
//				
//			}
			finalMethod();
		}
		
		public static void finalMethod() {
			final int a =10;
//			a = 20;
//			a= 30; 
		// We cannot change the value of the variable which is having final keyword before data type 	
			System.out.println(a);
			
		}
		

}
