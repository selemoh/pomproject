package javaConceptsIvw;

public abstract class Bank {
	
	// Abstract class is a class which is used to hide the implementation logic.
	// Abstract class can have abstract as well as non-abstract methods but at-least
	// one abstratc method needed to be declared in the abstract class
	// Due to non-abstarct methods this abstraction is called as partial abstraction.
	// Abstract class can have some logic in non-abstract methods
	
	// We cannot create the object of abstract class
	
	// In abstract class we can define any type of variables e.g. non-static, static and final variables etc.
	int rs = 100;
	static int loan = 1234;
	final int interest = 5;
	
	
	public abstract void loan();
	
	public void credit() {
		System.out.println("Loan amount - credited");
	}
	
	public void debit() {
		System.out.println("Emi amount - debited");
	}

}
