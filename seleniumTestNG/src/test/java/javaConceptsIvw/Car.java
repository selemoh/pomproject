package javaConceptsIvw;

public interface Car {

	// In interface we always define only abstarct methods
	// It helps us to hide the implementation logic 100% hence
	// to achieve full 100% abstraction
	// we don't need to write abstract here while defining method
	// We cannot create the object of interface
	// In interface class we can define only static and final type of variables
	// if we don't write final and static at the time of defining, the variable 
	// will be considered as static and final in nature.
	
	static int speed = 90;
	final int rpm = 2000;
	int rspm = 1200;
	
	public void start();
	public void stop();
	public void refuel();
	
}
