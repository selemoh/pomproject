package practiceTill21Jun;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TesNG_Concepts {
	
	WebDriver driver;
	
	/*
	 * @BeforeSuite
	 * @BeforeTest
	 * @BeforeClass
	 * @BeforeMethod
	 * @Test
	 * @AfterMethod
	 * @AfterClass
	 * @AfterTest
	 * @AfterSuite
	 * @Parameters 
	 * 
	 */
	
	// In this Test Class we are going to Run the Parameterization concept and DataProvider concept
	
	
	@BeforeMethod
	@Parameters({"browser"})
	public void initBrowser(String browser) {
		if(browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
		driver = new ChromeDriver();
		}
		
		else if(browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
		driver = new FirefoxDriver();
		}
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);		
	}
	
	@Test
	@Parameters({"url"})
	public void parametersTestCase(String url) throws InterruptedException {
		driver.get(url);
		
	}
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if (ITestResult.SUCCESS == result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test Case: " + result.getName() + " is PASSED");
			System.out.println(" ");
		}

		else if (ITestResult.FAILURE == result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test Case: " + result.getName() + " is FAILED");
			System.out.println(" ");
		}

		else if (ITestResult.SKIP == result.getStatus()) {
			System.out.println(" ");
			System.out.println("Test Case: " + result.getName() + " is SKIPPED");
			System.out.println(" ");
		}
		Thread.sleep(2000);
		driver.quit();
	}
}
