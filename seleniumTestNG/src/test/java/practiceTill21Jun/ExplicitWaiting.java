package practiceTill21Jun;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class ExplicitWaiting {

	WebDriver driver;
	
	@Test(priority=1)
	public void firstWait()throws InterruptedException {

	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
			+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");	
	
	driver = new ChromeDriver();
	
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	driver.get("https://www.crmpro.com/index.html");
	
	// Setting Explicit wait locally by using WebDriverWait class from selenium 
	
	WebDriverWait wait = new WebDriverWait(driver, 30);
		
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='username']"))).sendKeys("balujasmohit");	
		
		Thread.sleep(2000);
		System.out.println("Test Execution is finished of method: firstWait");
		driver.quit();		
	}
	
	public void clickOn(WebDriver driver, WebElement locateToClick, int timeout) {
		new WebDriverWait(driver, timeout).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(locateToClick));
		locateToClick.click();
	}
	public void passOn(WebDriver driver, WebElement locateToPass, int timeout, String newText) {
		new WebDriverWait(driver, timeout).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(locateToPass));
		locateToPass.sendKeys(newText);
	}

	// Setting Explicit wait by calling Parameterized methods into Test method
	// This can be created as a utility also.
	
	@Test(priority=2)
	public void secondWait() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");	
		
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(70, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("https://waypals.com/wpl/#LOGIN");
		
		Thread.sleep(2000);
		//Passing the text in username text field.
		passOn(driver, driver.findElement(By.xpath("//input[@type='text']")), 30, "balujasmohit");
		
		//Clickiing on an Login button
		clickOn(driver, driver.findElement(By.xpath("//button[@type='button']")), 30);
		
		Thread.sleep(3000);
		System.out.println("Test Execution is finished of method: secondWait");
		driver.quit();	
	}
}
