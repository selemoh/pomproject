package practiceTill21Jun;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class WebTableDynamic {
	
	WebDriver driver;
	
	@Test
	public void webTable1() throws InterruptedException {

//		Web Table handling by xpath concatenation in for loop, Actions class and JavascriptExecutor
//      And printing the data on console

		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://waypals.com/wpl/#LOGIN");

		driver.findElement(By.xpath("//input[@type='text']")).sendKeys("mohit@waypals.com");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("ajwaypals");
		driver.findElement(By.xpath("//button[@type='button']")).click();

		Thread.sleep(8000);

		// Handling Web Dynamic Web Table using Actions Class, JS executor and Java for
		// loop

		driver.findElement(By.xpath("//td[text()='CUSTOMER']")).click();
		Thread.sleep(3000);

		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollBy(0,450)", "");

		Thread.sleep(8000);

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath(
				"//input[@class='select-widget-caption select-widget-caption-readonly input_textarea left calendar-time-small']")));
		driver.findElement(By.xpath(
				"//input[@class='select-widget-caption select-widget-caption-readonly input_textarea left calendar-time-small']"))
				.click();
		driver.findElement(By.xpath("//a[contains(text(),'50')]")).click();
		
		js.executeScript("window.scrollBy(0,50)", "");

		Thread.sleep(3000);
		String first = "/html/body/div[1]/div[2]/div/div/div/div[2]/div/div[2]/div[2]/div[2]/table/tbody/tr[";
		String four = "]";

		for (int i = 2; i < 52; i++) {
			String rowValues = driver.findElement(By.xpath(first + i + four)).getText();
			System.out.println(rowValues);
		}
		Thread.sleep(1000);
		driver.quit();
	}
	
	/*
	 * @Test public void webTable2() throws InterruptedException {
	 * 
	 * // Web Table handling by xpath concatenation in for loop to click on checkbox
	 * 
	 * Thread.sleep(2000); System.setProperty("webdriver.chrome.driver",
	 * System.getProperty("user.dir") +
	 * "\\libraries\\chromedriver_win32\\chromedriver.exe"); driver = new
	 * ChromeDriver();
	 * 
	 * driver.manage().window().maximize(); driver.manage().deleteAllCookies();
	 * driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 * 
	 * driver.get("https://www.crmpro.com/index.html"); // First we need to close
	 * the chat modal pop up // It is in a frame so we will switch to that fram and
	 * close this pop up // We can switch to frame by index or by the frame name
	 * 
	 * Thread.sleep(8000); driver.switchTo().frame("intercom-borderless-frame");
	 * 
	 * // After this we need to perform mouse hover, so we will do it with the help
	 * of // selenium Actions class
	 * 
	 * // Action class is already declared in this method so we will use the
	 * reference // of that
	 * 
	 * Actions action = new Actions(driver);
	 * 
	 * action.moveToElement(driver.findElement(By.xpath(
	 * "//span[contains(text(),'CRM')]"))).build().perform();
	 * 
	 * driver.findElement(By.xpath(
	 * "//div[contains(@class,'intercom-borderless-dismiss-button')]")).click();
	 * 
	 * Thread.sleep(500);
	 * 
	 * driver.findElement(By.name("username")).sendKeys("balujasmohit");
	 * driver.findElement(By.name("password")).sendKeys("saiji@1983");
	 * driver.findElement(By.xpath("//input[@type='submit']")).click();
	 * 
	 * Thread.sleep(6000);
	 * 
	 * driver.switchTo().frame("mainpanel");
	 * 
	 * driver.findElement(By.xpath("//a[contains(text(),'Contacts')]")).click();
	 * 
	 * String mBefore = "//*[@id='vContactsForm']/table/tbody/tr[";
	 * 
	 * String mAfter = "]/td[2]/a";
	 * 
	 * // //*[@id='vContactsForm']/table/tbody/tr[7]/td[1]/input
	 * 
	 * for (int m = 4; m <= 7; m++) { String name =
	 * driver.findElement(By.xpath(mBefore + m + mAfter)).getText();
	 * System.out.println(name); if (name.contains("Narendra Singh")) {
	 * driver.findElement(By.xpath("//*[@id='vContactsForm']/table/tbody/tr[" + m +
	 * "]/td[1]/input")).click(); } else {
	 * System.out.println("Element is not available Yet"); } } Thread.sleep(5000);
	 * driver.quit(); }
	 * 
	 * @Test public void webTable3() throws InterruptedException {
	 * 
	 * // Handling web table by using customized xpath to click on check box.
	 * 
	 * Thread.sleep(2000); System.setProperty("webdriver.chrome.driver",
	 * System.getProperty("user.dir") +
	 * "\\libraries\\chromedriver_win32\\chromedriver.exe"); driver = new
	 * ChromeDriver();
	 * 
	 * driver.manage().window().maximize(); driver.manage().deleteAllCookies();
	 * driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 * 
	 * driver.get("https://www.crmpro.com/index.html"); // First we need to close
	 * the chat modal pop up // It is in a frame so we will switch to that fram and
	 * close this pop up // We can switch to frame by index or by the frame name
	 * 
	 * Thread.sleep(8000); driver.switchTo().frame("intercom-borderless-frame");
	 * 
	 * // After this we need to perform mouse hover, so we will do it with the help
	 * of // selenium Actions class
	 * 
	 * // Action class is already declared in this method so we will use the
	 * reference // of that
	 * 
	 * Actions action = new Actions(driver);
	 * 
	 * action.moveToElement(driver.findElement(By.xpath(
	 * "//span[contains(text(),'CRM')]"))).build().perform();
	 * 
	 * driver.findElement(By.xpath(
	 * "//div[contains(@class,'intercom-borderless-dismiss-button')]")).click();
	 * 
	 * Thread.sleep(500);
	 * 
	 * driver.findElement(By.name("username")).sendKeys("balujasmohit");
	 * driver.findElement(By.name("password")).sendKeys("saiji@1983");
	 * driver.findElement(By.xpath("//input[@type='submit']")).click();
	 * 
	 * Thread.sleep(6000);
	 * 
	 * driver.switchTo().frame("mainpanel");
	 * 
	 * driver.findElement(By.xpath("//a[contains(text(),'Contacts')]")).click();
	 * 
	 * WebDriverWait wait = new WebDriverWait(driver, 30);
	 * 
	 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
	 * "//a[contains(text(),'Narendra Singh')]/parent::td//preceding-sibling::td//input[contains(@type,'checkbox')]"
	 * ))) .click();
	 * 
	 * Thread.sleep(3000); System.out.println(" ");
	 * System.out.println("Web Table Handling class is working fine");
	 * System.out.println(" "); driver.quit(); }
	 */
}
