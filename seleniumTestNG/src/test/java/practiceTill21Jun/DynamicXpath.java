package practiceTill21Jun;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DynamicXpath {

	static WebDriver driver;
	
	public static void main(String[] args) throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("http://dev.waypals.com:9198/wpl/");
		
	    // Absolute Xpath for an element	
//		driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div[2]/input")).sendKeys("mohit@waypals.com");
		
	    // Relative xpath  //input[@type='text']
//		driver.findElement(By.xpath("//input[@type='text']")).sendKeys("mohit@waypals.com");
		
		// Customized xpath 1
		
//		driver.findElement(By.xpath("//input[contains(@type,'text')]")).sendKeys("mohit@waypals.com");
		
		// Customized xpath 2	
//		driver.findElement(By.xpath("//button[contains(text(),'SIGN')]")).click();

		// Customized xpath 3	
//		driver.findElement(By.xpath("//button[contains(@type,'button') and (text()='SIGN IN')]")).click();
		
		// Customized xpath 4
//		driver.findElement(By.xpath("//button[starts-with(text(),'SIGN')]")).click();
		
		// Customized xpath 5
		driver.findElement(By.xpath("//input[starts-with(@type,'pass')]")).sendKeys("waypals");		
		
		System.out.println("Test Execution completed");
				
		Thread.sleep(2000);
		driver.quit();
	}

}
