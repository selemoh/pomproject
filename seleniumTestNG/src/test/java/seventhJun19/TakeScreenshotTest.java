package seventhJun19;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TakeScreenshotTest {
	WebDriver driver;
	
	static Properties prop = new Properties();
	
	 static {
		try{
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
				+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
		prop.load(fis);
		}catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeMethod
	public void initBrowser() {
		String b = prop.getProperty("browser");
		if(b.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(b.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
     public void capturingScreens(String screenName) throws InterruptedException, IOException {
		
		driver.get("https://google.com");
		
		Thread.sleep(4000);
		
		DateFormat dateform = new SimpleDateFormat("dd-MMM-yyyy HH-mm-ss");
		Date date = new Date();
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		
		File src = ts.getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(src, new File("./screenshots/general/"+screenName+"_"+dateform.format(date)+".jpg"));
	}
	
	@Test(priority=1)
	public void screenshotTest() throws IOException, InterruptedException {
		capturingScreens("Google_Page");
		System.out.println("Screenshot is captured");	
	}
	
	public void clickOn(WebDriver driver, WebElement locator, int timeout) {
		new WebDriverWait(driver,timeout).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(locator));
		locator.click();
	}
	
	public void enterText(WebDriver driver, WebElement text, int timeout, String send) {
		new WebDriverWait(driver,timeout).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(text));
		text.sendKeys(send);
	}
	
	@Test(priority=2)
	public void dynamicWaits() {
		
		driver.get("http://dev.waypals.com:9198/wpl/#LOGIN");
		enterText(driver, driver.findElement(By.xpath("//input[@class='input_textarea']")), 20, "mohit@waypals.com");
		//  //input[@class='input_textarea']
		
		// WebDriverWait class
		clickOn(driver, driver.findElement(By.xpath("//button[@type='button']")), 30);	
		
	}
	
	
	@AfterMethod
	public void tearDown() throws InterruptedException{
		Thread.sleep(2000);
		driver.quit();
	}
}
