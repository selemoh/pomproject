package seventhJun19;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DynamicXpath {
	
	static WebDriver driver;
	
	public static void main(String []args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32_N\\chromedriver.exe");
	
			driver = new ChromeDriver();
			driver.manage().window().maximize();
	//		driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
			driver.get("https://waypals.com/wpl/#LOGIN");
			
			//  creating customize xpath using html tag and attribute
//  		driver.findElement(By.xpath("//input[@type='text']")).sendKeys("mohit@waypals.com");
//			System.out.println("Entered the text Successfully");
			
			//creating xpath using contains function
			// 1. simple xpath with html tag and attribute
//			driver.findElement(By.xpath("//input[contains(@class,'gwt-PasswordTextBox input_textarea')]")).sendKeys("mohit@waypals.com");
			
			//creating xpath using contains function
			// 1. Dynamic xpath with html tag and attribute
//			driver.findElement(By.xpath("//input[contains(@class,'input_')]")).sendKeys("mohit@waypals.com");
			
			//creating xpath using starts-with function
			// 1. simple xpath with html tag and attribute
//			driver.findElement(By.xpath("//button[starts-with(text(),'SIGN')]")).click();
			
			//creating xpath using starts-with function
			// 1. Dynamic xpath with html tag and attribute
//			driver.findElement(By.xpath("//a[starts-with(text(),'Terms &')]")).click();

			//creating xpath using ends-with function
			// 1. simple xpath with html tag and attribute
//			driver.findElement(By.xpath("//button[starts-with(text(),'SIGN')]")).click();
	
//           Ends with is not currently working			
			//creating xpath using ends-with function
			// 1. Dynamic xpath with html tag and attribute
//			driver.findElement(By.xpath("//a[starts-with(text(),'Terms &')]")).click();			
			
			// Clicking on a link 		
			driver.findElement(By.xpath("//a[contains(text(),'Forgot Password ?')]")).click();
			
			Thread.sleep(10000);
			driver.quit();
	}
}
