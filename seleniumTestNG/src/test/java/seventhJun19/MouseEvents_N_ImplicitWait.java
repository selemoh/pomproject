package seventhJun19;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MouseEvents_N_ImplicitWait {

	WebDriver driver;
	
	static Properties prop = new Properties();
	static {
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
					+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
			prop.load(fis);
		}catch(FileNotFoundException fnf) {
			fnf.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeMethod
	public void launchBrowser() {
		String browse = prop.getProperty("browser");
		if(browse.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", 
					"D:\\POMProject\\pomproject\\seleniumTestNG\\libraries\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(browse.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", 
					"D:\\POMProject\\pomproject\\seleniumTestNG\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
	}
	
	@Test(priority=1)
	public void mouseHover1() throws InterruptedException{
		driver.get("https://www.spicejet.com/");
		System.out.println(" ");
		String pageTitle = driver.getTitle();
		System.out.println("The title of current webpage is: "+pageTitle);
		System.out.println(" ");
		Actions action = new Actions(driver);
		// Mouse Hover on Dropdown List
		action.moveToElement(driver.findElement(By.id("ctl00_HyperLinkLogin"))).build().perform();
		// Clicking on an option from dropdown menu
		driver.findElement(By.xpath("//a[contains(text(),'Sub Agent Login (AGENT PLUS)')]")).click();
		String pageTitleNew = driver.getTitle();
		System.out.println("The title of current webpage is: "+pageTitleNew);
		System.out.println(" ");
	}
	
	@Test(priority=2)
	public void drag_N_Drop() throws InterruptedException{
		
		driver.get("https://jqueryui.com/droppable/");
		System.out.println(" ");
		String pageTitle = driver.getTitle();
		System.out.println("The title of current webpage is: "+pageTitle);
		System.out.println(" ");
		
		driver.switchTo().frame(0);
		Thread.sleep(2000);
		Actions action = new Actions(driver);
		// Performing drag and drop function
		action.clickAndHold(driver.findElement(By.id("draggable")))
		.moveToElement(driver.findElement(By.id("droppable")))
		.release()
		.build()
		.perform();
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException{
		if (ITestResult.SUCCESS==result.getStatus()){
			System.out.println("The testCase: "+result.getName()+" is Passed");
			System.out.println(" ");
		}
		
		else if(ITestResult.FAILURE==result.getStatus()){
			System.out.println("The testCase: "+result.getName()+" is Failed"); 
			System.out.println(" ");
			}
		
		Thread.sleep(4000);
		driver.quit();
	}
}
