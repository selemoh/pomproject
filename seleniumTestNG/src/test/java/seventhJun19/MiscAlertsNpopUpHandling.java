package seventhJun19;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MiscAlertsNpopUpHandling {
	
	WebDriver driver;
	
	static Properties prop = new Properties();
	
	static {
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
					+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
			prop.load(fis);
		}
		catch(FileNotFoundException fnf) {
			fnf.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeMethod
	public void initBrowser() {
		String browser = prop.getProperty("browser");
	
	if(browser.equals("chrome")) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	else if(browser.equals("firefox")) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
				+"\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}	
	
	@Test(priority=1)
	public void AlertspopUp() {
		driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("proceed"))).click();
		
		Alert alert = driver.switchTo().alert();
	    String alertText = alert.getText();
	    System.out.println("Text on alert pop up is: "+alertText);
	    
	    // To accept or Ok the the alert
	    alert.accept();
		/*
		 * To Reject or Cancel the alert alert.dismiss();
		 	alert.dismiss();
		 */
		}
	
	@Test(priority=2)
	public void handleFileUpload() throws InterruptedException {
		driver.get("https://html.com/input-type-file/");
		WebDriverWait wait  = new WebDriverWait(driver, 50);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@class,'pm-cta')]"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@id,'cn-accept-cookie')]"))).click();
		
		JavascriptExecutor js = (JavascriptExecutor)driver;	
		js.executeScript("window.scrollBy(0,700)", "");	
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='file' and @name='fileupload']")))
		.sendKeys(System.getProperty("user.dir")+"\\testData\\TestFileUpload.txt");
	Thread.sleep(1000);
	}
	
	@Test(priority=3)
	public void windowsHandle()throws InterruptedException {
		driver.get("http://www.popuptest.com/goodpopups.html");
		WebDriverWait wait  = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='black']"))).click();
		
		Set<String> handler = driver.getWindowHandles();
		Iterator<String> it = handler.iterator();
		
		String parentWindow = it.next();
		System.out.println("The Parent window id is: '"+parentWindow+"'");
		
		String childWindow = it.next();
		System.out.println("The Child window id is: '"+childWindow+"'");
		
		driver.switchTo().window(childWindow);
		String childWinTitle = driver.getTitle();
		System.out.println("Child window title is: "+childWinTitle);
		
		driver.close();
	
		driver.switchTo().window(parentWindow);
		String parentWinTitle = driver.getTitle();
		System.out.println("Child window title is: "+parentWinTitle);
		Thread.sleep(2000);
	}
	
	@AfterMethod
	public void resultNtearDown(ITestResult result) throws InterruptedException{
	if(ITestResult.SUCCESS==result.getStatus()) {
		System.out.println(" ");
	    System.out.println("The TestCase: "+result.getName()+" is PASSED");	
	    System.out.println(" ");
	}
	else if(ITestResult.SUCCESS==result.getStatus()) {
		System.out.println(" "); 
		System.out.println("The TestCase: "+result.getName()+" is FAILED");	
		System.out.println(" ");
	}
	Thread.sleep(2000);
	driver.quit();
	}
}
