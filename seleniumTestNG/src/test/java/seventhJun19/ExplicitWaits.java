package seventhJun19;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ExplicitWaits {

	WebDriver driver;
	
	static Properties prop = new Properties();

	static {
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
					+"\\src\\main\\java\\com\\wpl\\qa\\config\\conf.properties");
			prop.load(fis);
		}catch(FileNotFoundException fnf){
			fnf.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
	} 
	
	@BeforeMethod
	public void initBrowser() {
	
		String b = prop.getProperty("browser");
		
		if(b.equals("chrome")){
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
					+"\\libraries\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(b.equals("firefox")){
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
					+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	// Practicing new appraoch of Explicit wait and it is very much reusable.
	
	// Explicit wait for clickable WebElement
	public void clickOn(WebDriver driver, WebElement clickLocate, int timeout) {
		new WebDriverWait(driver, timeout).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(clickLocate));
		clickLocate.click();
	}
	
	// Explicit wait for input WebElement
		public void sendText(WebDriver driver, WebElement text, int timeout, String send) {
			new WebDriverWait(driver, timeout).ignoring(StaleElementReferenceException.class)
			.until(ExpectedConditions.elementToBeClickable(text));
			text.sendKeys(send);
		}
	
	// Implicit and Explicit waits are also known as dynamic waits	
		
	@Test
	public void explicitWaitTest() throws InterruptedException {
		
		driver.get("https://www.crmpro.com/index.html");
		
		Thread.sleep(6000);
		
		driver.switchTo().frame("intercom-borderless-frame");
		
		Actions action  = new Actions(driver);
		
		action.moveToElement(driver.findElement(By.xpath("//div[@class='intercom-block intercom-block-paragraph']"))).build().perform();
		
		driver.findElement(By.xpath("//div[@aria-label='Dismiss']")).click();
		
		Thread.sleep(5000);
		
		sendText(driver, driver.findElement(By.xpath("//input[@type='text']")), 30, "balujasmohit");
		// This is called a static wait
		Thread.sleep(2000);
		
		clickOn(driver, driver.findElement(By.xpath("//input[@type='submit']")), 30);
	}
		
	@AfterMethod
	public void tearDown(ITestResult result) throws InterruptedException {
		if(ITestResult.SUCCESS==result.getStatus()) {
			System.out.println("The Test case: "+result.getName()+" is PASSED");
		}
		
		else if(ITestResult.FAILURE==result.getStatus()) {
			System.out.println("The Test case: "+result.getName()+" is FAILED");
		}
		System.out.println(" ");
		System.out.println("Test Execution is completed");
		System.out.println(" ");
		Thread.sleep(2000);
		driver.quit();
	}
}
