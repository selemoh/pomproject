package seventhJun19;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class SimulationOfNavigation {
	
	WebDriver driver;
	
	@Test
	public void main() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", 
				"D:\\POmProject\\pomproject\\seleniumTestNG\\libraries\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	//	driver = new FirefoxDriver();
	// "D:\\POmProject\\pomproject\\seleniumTestNG\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");	
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("1");
		driver.get("https://www.google.com/");
		System.out.println("2");
		driver.navigate().to("https://www.facebook.com/");
		
		Thread.sleep(2000);
		// google
		System.out.println("3");
		driver.navigate().back();
		
		Thread.sleep(6000);
		// facebook
		System.out.println("4");
		driver.navigate().forward();
		
		Thread.sleep(12000);
		
	//	driver.navigate().back();
		// google
			driver.navigate().to("https://google.co.in");
			System.out.println("5");
	
			Thread.sleep(4000);
		driver.quit();
	}
	
	

}
