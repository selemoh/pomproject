package seventhJun19;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LinksCountandText {
	
	static WebDriver driver;
	
	public static void main(String []args) throws InterruptedException{
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")
				+"\\libraries\\geckodriver-v0.23.0-win32\\geckodriver.exe");
	
//			driver = new ChromeDriver();
			driver= new FirefoxDriver();
			driver.manage().window().maximize();
	//		driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
			driver.get("https://waypals.com/wpl/#LOGIN");
			
			Thread.sleep(3000);
			
			// Getting the count of links available on page
	
			List<WebElement> links = driver.findElements(By.tagName("a"));
			int count = links.size();
			System.out.println("The total number of links available on the page is: "+count);
			System.out.println(" ");
			System.out.println("Below are the links available on the page");
			
			// Getting the links text
			
			for(int i=0; i<links.size(); i++) {
				String linkstext = links.get(i).getText();
				System.out.println(linkstext);
				System.out.println(" ");
			}
			
			Thread.sleep(2000);
			driver.quit();
	}

}
