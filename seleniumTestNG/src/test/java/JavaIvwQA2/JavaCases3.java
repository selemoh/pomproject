package JavaIvwQA2;

public class JavaCases3 {

	public static void main(String[] args) {
		stringManipulation();
	}

	public static void stringManipulation() {
		// Getting the length of String    // Getting the index of a particular character - position of character
		// Getting the index of a particular character when the character is available at multiple place in a String.
		// Getting different occurances of the character   // Getting the character on a particular index - character on particular location.
		// Getting index of specific string or a word in text line of String.  // If the String is not available then we will get -1
		// String comparison with equals method - if string is equal then it returns true else it returns false.
		// String Comparison with equalsIgnoreCase method.  // Converting an Integer to String.  // Getting sub-string.
		// Trim function // Replace function   // Split function - in here we split a string / value on the basis of a character or symbol.
		// also split function returns an array of values so we need to store it in array.
		// Math.addExact function is used to give the index number or serial numbers to array output values
		// String.concates // String Concatenation - to merge or add two things.
		
		// Getting the length of String
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// Getting the index of a particular character - position of character
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// Getting different occurances of the character
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// Getting the character on a particular index - character on particular location.
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// Getting index of specific string or a word in text line of String.
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// String Comparison with equalsIgnoreCase method.
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// Trim Method
		
		// Replace Method
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// Split Method
		
		System.out.println("\n--------------------------------------------------------------------------------------------------");
		// String concatenation
				
		System.out.println("\n==================================================================================================");
	}
}
