package JavaIvwQA2;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class JavaCases4 {

	public static void main(String[] args) {

    	isArmstrongNumber(371);
    	System.out.println("\n--------------------------+++++++++++++++++++++++++++-------------------------------");
		isArmstrongNumber(0);
		System.out.println("\n--------------------------+++++++++++++++++++++++++++-------------------------------");
		isArmstrongNumber(3);
		System.out.println("\n--------------------------+++++++++++++++++++++++++++-------------------------------");
		// Non recursive method
		System.out.println("Factorial without recursive: "+factorial(5));
		System.out.println("Factorial without recursive: "+factorial(3));
		
		// Recursive method
		System.out.println("\nFactorial of the given number of recursive method is: "+facts(3));
		System.out.println("\nFactorial of the given number is: "+facts(7));
		
		System.out.println("\n--------------------------+++++++++++++++++++++++++++-------------------------------");
		
		System.out.println("\n"+isOnlyDigits("abc123"));
		System.out.println("\n"+isOnlyDigits("243566"));
		System.out.println("\n"+isOnlyDigits("tetetrtr"));
		System.out.println("\n"+isOnlyDigits("%$&$&$*"));
		System.out.println("\n--------------------------+++++++++++++++++++++++++++-------------------------------");
		
		duplicateEntries("Hello this is not a Hello String and working on Hello this is");
	}

	public static void isArmstrongNumber(int num) {
		System.out.println("\nGiven number is '"+num+"'");
		int tub = 0;
		int r;
		int t;
		 t=num;
		 
		 while(num>0) {
			 r=num%10;
			 num = num /10;
			 tub  = tub + (r*r*r);
		 }
		 if(t==tub) {
			 System.out.println("This number is an armstrong");
		 }else {
			 System.out.println("This number is not an armstrong");
		 }
	}
	
// Finding factorial of Given number(s)
	
	// Without ecursive method  -- using for loop
	
	public static int factorial(int num) {
		System.out.println("\nGiven number is "+num);
		int fact=1;
		if(num==0) {
			return 1;
		}
		for(int i=1; i<=num; i++) {
			fact =  fact*i;
		}
		return fact;
	}
	
    // With recursive method
	public static int facts(int num) {
		
		if(num==0) 
			return 1;
		else
		return (num*facts(num-1));
	}
	
// Verify that the String is only having digits	
	
	public static boolean isEmpty(CharSequence cs) {
		return cs==null || cs.length()==0;
	}
	
	public static boolean isOnlyDigits(CharSequence cs) {
	      if(isEmpty(cs)) {
	    	  return false;
	      }
	      else {
	    	  for(int i=0; i<cs.length(); i++) {
	    		  if(! Character.isDigit(cs.charAt(i))) {
	    			  return false;
	    		  }
	    	  }
	    	  return true;
	      }
	}
	
//  Finding the duplicate words and occurences in a string
	
	public static void duplicateEntries(String str) {
		System.out.println("\nGiven String is: "+str);
		System.out.println(" ");
		String [] words = str.split(" ");
	
	// Declaring HashMap of java utils to store the duplicate values
	
		Map<String, Integer> repeatWords = new HashMap<>();
	// Declaring for each loop and refeeing the words array to it	
		for (String word : words) {
	// Declaring an Integer variable refrence for the HashMAp
		Integer count = repeatWords.get(word); // no value is currently available in HashMap so putting a condition
		// in next line
		if(count == null) {
			repeatWords.put(word, 1);
		}else {
			repeatWords.put(word, ++count);
		}
			
		}
		
		// Declaring an Entry set reference to get a Set view of mappings of this map
		
		Set<Entry<String, Integer>> entrySet = repeatWords.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			if(entry.getValue()>1) {
				System.out.println("Duplicate word: '"+entry.getKey()+"' with Occurences count: '"+entry.getValue()+"'");
			}
		}
	}
	
	
}
