package JavaIvwQA2;

import java.util.Arrays;

public class JavaCases2 {
	
	public static void main(String []args) {
		// 6. FindLargestAndSmalletNumberInArray
		FindSmallestAndLargetsNumberInArray();  // Min and Max number
		// Swapping integer values with or without third temporary variable
		swapNumbers();
		// Swapping String values with or without third temporary variable
		swapStringValues();
		
		//Finding Prime Numbers
		getPrimeNumbers2();
		
		// Verifying a number is palindrome
		// Corner case of zero
		isPalindromeNum(121);
		isPalindromeNum(121);
		isPalindromeNum(12232);
		isPalindromeNum(131);
		isPalindromeNum(1221);
		
		isPalindromeStr("fleelf");
		isPalindromeStr("fleellf");
		isPalindromeStr("fleelf");
		isPalindromeStr("madam");
		isPalindromeStr("madame");
		
		isPrimeNum(2);
		isPrimeNum(3);
		isPrimeNum(4);
		isPrimeNum(6);
		isPrimeNum(7);
	}
		
	public static void FindSmallestAndLargetsNumberInArray() {
		
		int number[] = {43, 1, 10, 9, 45, 656, -25, 8787};
		
		int large = 0;
		int small = 0;
		
		for (int i = 1; i<number.length; i++) {
			if(number[i]>large) {
				large = number[i];
			}
			else if(number[i]<small) {
				small = number[i];
			}
		}
		
		System.out.println("\nGiven array is :::  "+ Arrays.toString(number));
		System.out.println("\nThe largest number  :: "+large);
		System.out.println("\nThe smallest number :: "+small);
	}
	
	
	public static void swapNumbers() {
		System.out.println("\n===============================numbers swapping===============================================");
	
		int x  =20;
		int y = 30;
		
// method 1		
//		int z;
//		z= x; // 20
//		x = y; // 30
//		y = z; // 20
// Method 2		
		/* x = x +y; // 20 + 30 = 50 
		 * y= x -y; // 50 -30 = 20 
		 * x = x-y; // 50 - 20 = 30
		 */		
// Method 3
		
		x= x*y;    // 600
		y = x /y;  // 20
		x = x/y;   // 30
		
		
		System.out.println("\nValue of x after swapping :: "+x);
		System.out.println("\nValue of y after swapping :: "+y);
	}
	
    public static void swapStringValues() {
    	System.out.println("\n===============================String swapping===============================================");
    	String c= "Hello"; //length is 5(5 letters starting from index 0)
    	String d = "World"; // length is 5(5 letters starting from index 0)
    	
    	System.out.println("\nThe value of c before swapping : "+c);
    	System.out.println("\nThe value of d before swapping : "+d);
    	
    	System.out.println("\n+++---------------------++++++++++++++++++---------------------------+++++++++++++++++------------------");
    	// Appended the strings
    	c = c + d; // HelloWorld 9(10 letters starting from index 0)
    	d = c.substring(0, (c.length()-d.length())); // HelloWorld(10) - World(5) = 5 - Substring is - Hello(index 0 to 4 - i.e 5 letters)  
    	c = c.substring(d.length()); // Substring is World (index will begin from 5 as the length of d is 5 letters)
    	
    	System.out.println("\nThe value of c after swapping : "+c);
    	System.out.println("\nThe value of d after swapping : "+d);
	}
    
 //   Verifying a number is prime or not
 //  Getting all prime numbers from an integer value
    
    public static boolean isPrimeNum(int num) {
    	// We need to put the conditions to cover the test cases
    	if(num<=1) { // Prime number should not be equal to or less than 1
    		System.out.println("\nGiven number is not a prime number");
    		return false;
    	}
    	for (int i = 2; i<num; i++) {
    		if(num % i== 0) { // remainder of number should not be equal to zero
    			System.out.println("\nGiven number is not a prime number");
    			return false;
    		}
    	}
    	System.out.println("\nGiven number is a prime number");
    	return true;
    }
    
    public static void getPrimeNumbers(int num) {
    	
    }
    public static void getPrimeNumbers2() {
    	
    }
    
    public static void isPalindromeNum(int num) {
    	int r =0;
    	int sum= 0;
    	int t;
    	t=num;
    	while(num > 0) {
    		r = num%10;
    		sum = (sum *10) + r;
    		num = num/10;
    	}
    	if(t==sum) {
    		System.out.println("\nThis number "+sum+" is a palindrome");
    	}else {
    		System.out.println("\nThis number "+sum+" is not a palindrome");
    	}
    	
    System.out.println("\n*************************************************************************************************************");	
    } 
    
    public static void isPalindromeStr(String st) {
    
    	String s = "";
    	int lth = st.length();
    	for (int i = lth-1; i>=0; i--) {
    		s = s + st.charAt(i);
    	}
    System.out.println("\nThe reverse of String is: '"+s+"'");
    
    if(s.equals(st)) {
    	System.out.println("\nThis String is a palindrome");
    }else {
    	System.out.println("\nThis String is not a palindrome");
    }
    }
}

/*
 * public static void stringManipulation() { System.out.
 * println("\n=====================================String Manipulation========================================"
 * );
 * 
 * String a = "Get the length of String in Selenium Software"; String b =
 * "Get the length of String in Selenium Software"; String c =
 * "Get the Length of String in Selenium Software"; int d = 45; // Getting the
 * length of String System.out.println("\nLength of string : "+a.length());
 * System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Getting the index of a particular character - position of character
 * System.out.println("\nIndex of character 'l': "+a.indexOf('l'));
 * System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Getting the index of a particular character when the character is
 * available at multiple place in a String.
 * System.out.println("\nFirst Occurance of 'S' : "+a.indexOf('S')); // 1st
 * occurance System.out.println("\nSecond Occurance of 'S' : "+a.indexOf('S',
 * a.indexOf('S')+1)); // 2nd Occurance
 * System.out.println("\nThird Occurance of 'S' : "+a.indexOf('S',a.indexOf('S',
 * a.indexOf('S')+1)+1)); // 3rd Occurance System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Getting different occurances of the character // Getting the character
 * on a particular index - character on particular location.
 * System.out.println("\nThe character at location 10 is: "+a.charAt(10));
 * System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Getting index of specific string or a word in text line of String.
 * System.out.println("\nThe index position of 'String is: '"+a.indexOf("String"
 * )); System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // If the String is not available then we will get -1 System.out.
 * println("\nThe index position of non existing string ' Hello' is: "+a.indexOf
 * ("Hello")); System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * );
 * 
 * // String comparison with equals method - if string is equal then it returns
 * true else it returns false. // String Comparison with equalsIgnoreCase
 * method. System.out.println("\nString 'a' is equals to 'b' : "+a.equals(b));
 * System.out.println("\nString 'a' is equals to 'c' in normal case: "+a.equals(
 * c)); System.out.println("\nString 'a' is equals to 'c' withIgnoreCase: "+a.
 * equalsIgnoreCase(c)); System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Converting an Integer to String.
 * System.out.println("\nThe value of integer 'd' after conversion : "+String.
 * valueOf(d)); System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Getting sub-string. System.out.println("\n"+a.substring(0, 10));
 * System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Trim function // Replace function String tm =
 * "            The Ramayana is a Hindu Religion Holy Book              ";
 * System.out.println(tm.trim()); String rim = "Lord Ram"; rim =
 * rim.replace(" ", ""); System.out.println("\n"+rim); System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // Split function - in here we split a string / value on the basis of a
 * character or symbol String st = "The_new_movie_will_be_released_on_friday";
 * String strings[] = st.split("_"); for(int i =0; i<strings.length; i++) {
 * System.out.println("\nThe separated words of string are -- "+Math.addExact(i,
 * 0)+": "+strings[i]); } System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); // also split function returns an array of values so we need to store it
 * in array. // Math.addExact function is used to give the index number or
 * serial numbers to array output values // String.concates // String
 * Concatenation - to merge or add two things. String bd = "The final proces";
 * System.out.println("\nThe string 'bd' before concatenation : "+bd);
 * System.out.println("\nThe string 'bd' after concatenation : "+bd.concat("s"))
 * ; System.out.println(
 * "\n-------------------------------------------------------------------------------------"
 * ); }
 */
