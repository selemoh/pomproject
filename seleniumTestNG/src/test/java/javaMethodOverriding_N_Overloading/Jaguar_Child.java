package javaMethodOverriding_N_Overloading;

public class Jaguar_Child extends Car_Parent {
	
//	@Override
	public static void start() {
		System.out.println("Jaguar ------ Starts");
	}
	
	// We will get an error because we cannot override a static method of Parent class
	
}
