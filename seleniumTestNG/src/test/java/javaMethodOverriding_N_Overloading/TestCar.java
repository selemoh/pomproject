package javaMethodOverriding_N_Overloading;

public class TestCar {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {

		Jaguar_Child jc = new Jaguar_Child();
		jc.start(); // This is an overridden method which is getting called from Jaguar_Child class
		jc.stop();
		jc.refueling();
		Car_Parent.start(5);
		Car_Parent.start("Jaguar");
		
		
		// We can achieve Polymorphism by method overloading
	}

}
