package javaMethodOverriding_N_Overloading;

public class Car_Parent {

	//Created non static method for inheritance or Overriding concept
	
	// We can achieve method hiding by making method static and wont get the values at runtime.
	public static void start() {
		System.out.println("Car ------ Starts");
	}
	
	public static void start(int a) {
		System.out.println("Car ------ Starts2");
		System.out.println("\nThe total number of vehicles started is: "+a);
		
	}
	
	public static void start(String b) {
		System.out.println("Car ------ Start3");
		System.out.println("\nAll the started vehicles manufactured by: "+b);
	}
	
	
	public void stop() {
		System.out.println("Car ------ Stops");
	}
	
	public void refueling() {
		System.out.println("Car ------ refueled");
	}
}
