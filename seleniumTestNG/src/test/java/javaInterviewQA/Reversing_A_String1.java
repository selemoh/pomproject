package javaInterviewQA;

public class Reversing_A_String1 {

	public static void main(String[] args) {
		
		// How to Reverse a String / A. By using the below logics
		
		// Difference between String and StringBuffer Class
		// A. String is immutable Class so it doesn't have reverse function while StringBuffer is Mutable Class 
		// so it is having reverse function available
		
		//Do we have reverse function in String in String - No
		
		// Reversing String by using for loop / Own logic
		
		String s = "Selenium Test";
		  
		  int lth = s.length() ;
		  
		  String val = "";
		  
		  for(int i=lth-1; i>=0; i--) { 
			  val = val+s.charAt(i); 
			  }
		  System.out.println(val);
		 
		
		//  Reversing String by using StringBuffer Class object
		
		StringBuffer sb = new StringBuffer(s);
		System.out.println(sb.reverse());
		
		System.out.println(" ");
		String jkl = "edoc ni esrever si gnirtS sihT";
		
		int sd = jkl.length();
		
		String valu = "";
		
		for(int i = sd-1; i>=0; i--) {
			valu  = valu + jkl.charAt(i);
		}
		System.out.println(valu);
		System.out.println(" ");
		
		String h = "esrever gniogno na osla si sihT";
		
		StringBuffer bh = new StringBuffer(h).reverse();
		System.out.println(bh);
		
		System.out.println(" ");
		System.out.println("----------------++++++++++++++++++++++++++++++++++++++++++--------------------------");
		String ets = "enif gnikrow si ecivres ehT";
		
		int ds = ets.length();
		
		String dfg = "";
		
		for (int k = ds-1; k>=0; k--) {
			dfg = dfg + ets.charAt(k);
		}
		System.out.println(" ");
		System.out.println(dfg);
		
		System.out.println(" ");
		String jlk = "doog gninnur dna pu si revres ehT";
		
		StringBuffer sds = new StringBuffer(jlk).reverse();
		System.out.println(sds);
			
	}

}
