package javaInterviewQA;

public class FindArmstrongNumber {

	// An Armstrong number is a number that is the sum of its own digits each raised to the power of the number of digits.
	
	public static void isArmstrongNum(int num) {
		System.out.println("\nGiven number is '"+num+"'");
		int cube = 0;
		int r;
		int t;
		
		t=num;
		
		while(num>0) {
			r =num % 10; // getting the modulus of the passed number
			num = num / 10; // Decreasing the value of the passed number
			cube = cube + (r*r*r);
		}
		if(t==cube) {
			System.out.println("This is an Armstrong number");
		}
		else {
			System.out.println("This is not an Armstrong number");
		}
	}
	
	public static void main(String[] args) {

		isArmstrongNum(153);
		isArmstrongNum(232);
		isArmstrongNum(407);
		isArmstrongNum(0);
		isArmstrongNum(1);
		isArmstrongNum(1221);
	}
}
