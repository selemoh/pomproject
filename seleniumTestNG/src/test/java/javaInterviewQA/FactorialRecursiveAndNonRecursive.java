package javaInterviewQA;

public class FactorialRecursiveAndNonRecursive {

	
	// Factorial of 0 is always 1
	// fact 0 = 1
	// fact 1 = 1
	// fact 2 = 2*1=2
	// fact 3 = 3*2*1 = 6
	// fact 4 = 4*3*2*1 = 24
	// fact 5 = 5*4*3*2*1 = 120
	
	// Without Recursive method - -  using for loop
	
	public static int factorial(int num) {
		System.out.println("\nGiven number is '"+num+"'");
		int fact = 1;
		if(num==0) {
			return 1;
		}
//		for(int i=1; i<=num; i++) {
//			fact = fact*i;//1*2*3*4
//		}
		for(int i=4; i>0; i--) {
			fact = fact*i;//4
		}
		return fact;
	}
	
	
	// With Recursive method  - - is function which is calling itself
	
	public static int facts(int num) {
		
		if(num==0)
			return 1;
		else
			return (num * facts(num-1));
	}
	
	public static void main(String[] args) {

		System.out.println("The factorial of given number is = "+factorial(4));
		System.out.println("The factorial of given number is = "+factorial(0));
		System.out.println("The factorial of given number is = "+factorial(1));
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("The factorial of given number '0' is = "+facts(0));
		System.out.println("The factorial of given number '1' is = "+facts(1));
		System.out.println("The factorial of given number '5' is = "+facts(5));
		
	}

}
