package javaInterviewQA;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class DifferentWaysOfIteratingArrayList {
	
	// for loop, for each loop, iterartor-while loop, lambaexpression-for each loop, iterator-lambda-forEachRemaining loop, 
	// ListIterator-while loop

	public static void forLoop() {
		System.out.println("------------------------forLoop---------------------------------------------");
		ArrayList<String> myFilms = new ArrayList<String>();
		myFilms.add("Geet");
		myFilms.add("Yaarana");
		myFilms.add("Kaho Naa..Pyar Hai");
		myFilms.add("Kuch Kuch Hota hai");
		myFilms.add("Dilwale Dulhania Le Jayenge");
		
		for(int i=0; i<myFilms.size(); i++) {
			System.out.println(myFilms.get(i));
		}
	}
	
	public static void forEachLoop() {
		System.out.println("-----------------------forEachLoop----------------------------------------------");
		ArrayList<String> myFilms = new ArrayList<String>();
		myFilms.add("Geet");
		myFilms.add("Yaarana");
		myFilms.add("Kaho Naa..Pyar Hai");
		myFilms.add("Kuch Kuch Hota hai");
		myFilms.add("Dilwale Dulhania Le Jayenge");
		
		for(String show : myFilms) {
			System.out.println(show);
		}
	}
	
	public static void iteratorNForEachLoopForward() {
		System.out.println("------------------------iteratorNForEachLoopForward---------------------------------------------");
		ArrayList<String> myFilms = new ArrayList<String>();
		myFilms.add("Geet");
		myFilms.add("Yaarana");
		myFilms.add("Kaho Naa..Pyar Hai");
		myFilms.add("Kuch Kuch Hota hai");
		myFilms.add("Dilwale Dulhania Le Jayenge");
		
		Iterator<String> it = myFilms.iterator();
		
		while(it.hasNext()) {
//			String mt = it.next();
			System.out.println(it.next());
		}
	}
	
	public static void listIteratorNForEachLoopBackward() {
		System.out.println("---------------------------listIteratorNForEachLoopBackward----------------------------------------");
		ArrayList<String> myFilms = new ArrayList<String>();
		myFilms.add("Geet");
		myFilms.add("Yaarana");
		myFilms.add("Kaho Naa..Pyar Hai");
		myFilms.add("Kuch Kuch Hota hai");
		myFilms.add("Dilwale Dulhania Le Jayenge");
		
		ListIterator<String> pt = myFilms.listIterator(myFilms.size());
		
		while(pt.hasPrevious()) {
			String value = pt.previous();
			System.out.println("\n"+value);
		}	
	}
	
	public static void lambdaExpNForEachloop() {
		System.out.println("-----------------------------lambdaExpNForEachloop-----------------------------");
		ArrayList<String> myFilms = new ArrayList<String>();
		myFilms.add("Geet");
		myFilms.add("Yaarana");
		myFilms.add("Kaho Naa..Pyar Hai");
		myFilms.add("Kuch Kuch Hota hai");
		myFilms.add("Dilwale Dulhania Le Jayenge");
		
		myFilms.forEach(show -> {
			System.out.println(show);
		}
		);
	}
	
	public static void lambdaExpNForEachRemainingLoop() {
		System.out.println("---------------------------lambdaExpNForEachRemainingLoop----------------------------------");
		ArrayList<String> myFilms = new ArrayList<String>();
		myFilms.add("Geet");
		myFilms.add("Yaarana");
		myFilms.add("Kaho Naa..Pyar Hai");
		myFilms.add("Kuch Kuch Hota hai");
		myFilms.add("Dilwale Dulhania Le Jayenge");
		
		Iterator<String> it = myFilms.iterator();
		it.forEachRemaining(show ->{
			System.out.println(show);
		});
	}
	
	public static void main(String[] args) {
		forLoop();
		forEachLoop();
		iteratorNForEachLoopForward();
		listIteratorNForEachLoopBackward();
		lambdaExpNForEachloop();
		lambdaExpNForEachRemainingLoop();
	}
}
