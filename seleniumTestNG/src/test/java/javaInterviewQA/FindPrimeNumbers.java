package javaInterviewQA;

public class FindPrimeNumbers {
	
	// prime number is number which can be divided by itself only
	
	// First smallest prime number is 2
	// Second smallest prime number is 3
	
// Verifying a number is prime or not
	
	public static boolean isPrimeNum(int num) {
		if(num<=1) {  // cases - The number should not be equal to 1 or less than 1 
			return false;
		}
		
		for (int i=2; i<num; i++) {
			if(num % i == 0) {  // The remainder of the number should not be zero
				return false;
			}
		}
		return true;
	}
	
// Getting prime numbers from a range or a two digit number
	
	public static void getPrimeNumbers(int num) {
		for(int j = 2; j<=num; j++) {
			if(isPrimeNum(j)) {              // Declared a for loop starting with the minimum prime number of the integer value
				                             // calling boolean method in if condition to verify that the number meets the conditions
				System.out.println(j + "");
			}
		}
	}
	
	
	public static void main(String[] args) {

		System.out.println("\nNumber '2' is a prime number:  "+isPrimeNum(2));
		System.out.println("\nNumber '5' is a prime number:  "+isPrimeNum(5));
		System.out.println("\nNumber '8' is a prime number:  "+isPrimeNum(8));
		System.out.println("\nNumber '10' is a prime number:  "+isPrimeNum(10));
		System.out.println("\nNumber '15' is a prime number:  "+isPrimeNum(15));
		System.out.println("\nNumber '17' is a prime number:  "+isPrimeNum(17));
		
		System.out.println(" ");
		System.out.println("Prime numbers in range 10 are: ");
		getPrimeNumbers(10);
		System.out.println(" ");
		System.out.println("Prime numbers in range 15 are: ");
		getPrimeNumbers(15);
		System.out.println(" ");
		System.out.println("Prime numbers in range 20 are: ");
		getPrimeNumbers(20);
		System.out.println(" ");
		System.out.println("Prime numbers in range 25 are: ");
		getPrimeNumbers(25);
	}

}
