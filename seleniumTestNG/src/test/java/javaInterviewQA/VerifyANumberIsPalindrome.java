package javaInterviewQA;

public class VerifyANumberIsPalindrome {

// We will write a function / logic and call it in Main method
	
	public static void isPalindromeNum(int num) {
		System.out.println("\nGiven number is: "+ num);
		int s = 0;
		int sum = 0;
		int t;
		
		t = num;
		
		while(num>0) {
			s = num % 10;
			sum =  (sum*10)+s;
			num = num / 10;
		}
		
		if (t==sum) {
			System.out.println("The number is palindrome");
		}
		else {
			System.out.println("The number is not palindrome");
		}
	}
		
	public static void main(String[] args) {
		isPalindromeNum(161);
		isPalindromeNum(162);
		isPalindromeNum(151);
		isPalindromeNum(1);
		isPalindromeNum(0);
		isPalindromeNum(78687);
	}
}
