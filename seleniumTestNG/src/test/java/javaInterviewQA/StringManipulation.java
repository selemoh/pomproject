package javaInterviewQA;

public class StringManipulation {

	public static void main(String[] args) {
		
		// A String is the collection fo different characters which is written within double quotes.
		// The first index will start from 0.
		
// Getting the length of String		
// Getting the index of a particular character - position of character
// Getting the index of a particular character when the character is available at multiple place in a String.	
// Getting the character on a particular index - character on particular location.
// Getting different occurances of the character		
// Getting index of specific string or a word in text line of String
// If the String is not available then we will get -1		
// String comparison with equals method - if string is equal then it returns true else it returns false
// String Comparison with equalsIgnoreCase method		
// Converting an Integer to String	
// Getting sub-string
// Trim function
// Replace function
// Split function - in here we split a string / value on the basis of a character or symbol
// also split function returns an array of values so we need to store it in array.		
// Math.addExact function is used to give the index number or serial numbers to array output values
// String.concates		
// String Concatenation - to merge or add two things		

		String stt = "New string is going to be tested now.";
		String stt1 = "New string is going to be tested now.";
		String stt2 = "New string is Going to be tested now.";
		// int yytr = 12345;
		// Getting the length of String
		System.out.println("\n" + stt.length());
		// Getting the character on a particular index
		System.out.println("\nThe character at location 4 is :: " + stt.charAt(4));

		// Getting the index of a particular character
		System.out.println("\nThe First Occurance of 's' is at " + stt.indexOf('s') + "th position"); // 1st occurance
		// Getting the index of a particular character when the character is available
		// at multiple place in a String.
		// Second Occurance of character s
		System.out.println(
				"\nThe Second Occurance of 's' is at " + stt.indexOf('s', stt.indexOf('s') + 1) + "th position");
		// Third Occurance of character s
		System.out.println("\nThe Third Occurance of 's' is at "
				+ stt.indexOf('s', stt.indexOf('s', stt.indexOf('s') + 1) + 1) + "th position");

		// Getting index of specific string or a word in text line of String
		System.out.println("\nThe index position of specific word starts from :: " + stt.indexOf("going"));

		// The index of not available String
		System.out.println("\nThe index position of specific word starts from :: " + stt.indexOf("kamal"));

		System.out.println("\n" + stt.equals(stt1));

		System.out.println("\n" + stt.equals(stt2));
		// equals Ignore case
		System.out.println("\n" + stt.equalsIgnoreCase(stt2));

		// Getting the sub-string means the small string from a big one
		System.out.println("\n" + stt.substring(0, 25));

		// Trim function - It is generally used to trim the before space and after space
		// in a String
		String gd = "    Hello Duniya    ";
		System.out.println("\nBefore trim :::: " + gd);
		System.out.println("\nAfter trim1 :::: " + gd.trim());

		// Replace function - It is generally used to replace any character in string
		// and we can remove the between space also with
		// replace function
		System.out.println("\nRemoved the between space with replace function ::: " + gd.replace(" ", ""));
		String dt = "01-01-2019";
		System.out.println("\nBefore replacing ::: " + dt);
		System.out.println(" ");
		System.out.println("After replacing ::: " + dt.replace("-", "/"));

		// Split function

		String name = "Hello_My_World_Of_Fun";
		String names[] = name.split("_");
		for (int j = 0; j < names.length; j++) {
			System.out.println("\n" + Math.addExact(j, 1) + ": " + names[j]);
		}

		// String Concatenation
		String y = "Hello Friend";
		System.out.println("\n" + y.concat("s"));

		String x = "Hello";
		String z = "Friend";
		int v = 100;
		int w = 200;
		System.out.println("\n" + x + z);
		System.out.println(v + w);
		System.out.println(v + w + x + z);
		System.out.println("\n" + x + z + v + w);
		System.out.println("\n" + x + z + (v + w));

		// Strings can be concatenated with any data type
		// '+' sign is called string concatenator

		// Converting an Integer to String
		// System.out.println("\n" + String.valueOf(yytr));
	}
}
