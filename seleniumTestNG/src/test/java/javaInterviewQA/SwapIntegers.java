package javaInterviewQA;

public class SwapIntegers {

	public static void main(String[] args) {
// Swapping the values
		int x = 5; // 0101
		int y = 10; // 1010  -- 1111
	// 1st Method - Using third integer variable	
		/* int t ;
		 * t = x; 
		 * x = y; 
		 * y = t;
		 */
		// Using arithmatic operators
	// 2nd Method - using + and - operators	
		
		/*
		 * x = x+y; // 15 New value of x 
		 * y = x-y; // 15-10 = 5 new value of y 
		 * x = x-y; // 15 -5 = 10 new value of x
		 */		
		
	// 3rd Method - using * and / operators	
		
//		x = x * y; // New value = 50
//		y = x / y; // new value = 5
//		x = x / y; // new value = 10
		
	// 4th Method - using XOR operator	
		
		x = x ^ y; // value 15  0101 1010 = 1111
		y = x ^ y; // value 5   1111 1010 = 0101
		x = x ^ y; // value 10  1111 0101 = 1010 
		
		
		System.out.println("\n"+x);
		System.out.println("\n"+y);
		
	}

}
