package javaInterviewQA;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FindDuplicateWordsInString {

	public static void main(String[] args) {
		duplicateValues("My new task is the task is the typing task");
	}
	
	public static void duplicateValues(String str) {
		String [] words = str.split(" ");
		
		Map<String, Integer> dupWords = new HashMap<>();
		for (String word : words) {
			Integer count = dupWords.get(word);
			if(count == null) {
				dupWords.put(word, 1);
			}else {
				dupWords.put(word, ++count);
			}
		}
		
		Set<Entry<String,Integer>> entrySet = dupWords.entrySet();
		for(Entry<String, Integer> entry : entrySet) {
			if (entry.getValue()>1) {
				System.out.println("\nThe duplicate word is: '"+entry.getKey()+"' and Occurences are: "+entry.getValue());
			}
		}
	}
}
