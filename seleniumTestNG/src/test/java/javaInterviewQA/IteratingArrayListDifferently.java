package javaInterviewQA;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class IteratingArrayListDifferently {
	
	public static void forLoop() {
		System.out.println("-----Printing with for loop-----");
		ArrayList<String> myFriends = new ArrayList<String>();
		myFriends.add("Sunil");
		myFriends.add("Vikas");
		myFriends.add("Amit");
		myFriends.add("Mathew");
		myFriends.add("Sergei");
		
		for(int i=0; i<myFriends.size(); i++) {
			System.out.println(myFriends.get(i));
		}
	}
	
	public static void forEachLoop() {
		System.out.println("\n-----Printing with for each loop-----");
		ArrayList<String> myFriends = new ArrayList<String>();
		myFriends.add("Sunil");
		myFriends.add("Vikas");
		myFriends.add("Amit");
		myFriends.add("Mathew");
		myFriends.add("Sergei");
		
		for(String play : myFriends) {
			System.out.println(play);
		}
	}
	
	public static void iteratorWhileLoop() {
		System.out.println("\n-----Printing with Iterator and While Loop-----");
		ArrayList<String> myFriends = new ArrayList<String>();
		myFriends.add("Sunil");
		myFriends.add("Vikas");
		myFriends.add("Amit");
		myFriends.add("Mathew");
		myFriends.add("Sergei");
		
		Iterator<String> it = myFriends.iterator();
		
		while(it.hasNext()) {
			String show = it.next();
			System.out.println(show);
		}
	}
	
	public static void listIteratorwhileLoop() {
		System.out.println("\n-----Traversing and Printing bottom to top values with ListIterator and While Loop-----");
		ArrayList<String> myFriends = new ArrayList<String>();
		myFriends.add("Sunil");
		myFriends.add("Vikas");
		myFriends.add("Amit");
		myFriends.add("Mathew");
		myFriends.add("Sergei");
		
		ListIterator<String> lt = myFriends.listIterator(myFriends.size());
		while(lt.hasPrevious()) {
			String display = lt.previous();
			System.out.println(display);
		}
	}
	
	public static void lambdaNForEachLoop() {
		System.out.println("\n-----Printing with lambdaNForEachLoop-----");
		ArrayList<String> myFriends = new ArrayList<String>();
		myFriends.add("Sunil");
		myFriends.add("Vikas");
		myFriends.add("Amit");
		myFriends.add("Mathew");
		myFriends.add("Sergei");
	
		myFriends.forEach(view ->{
			System.out.println(view);
		});
	}
	
	public static void lambdaNForEachRemainingLoop() {
		System.out.println("\n-----Printing with lambda, Java8 And ForEachRemainingLoop-----");
		ArrayList<String> myFriends = new ArrayList<String>();
		myFriends.add("Sunil");
		myFriends.add("Vikas");
		myFriends.add("Amit");
		myFriends.add("Mathew");
		myFriends.add("Sergei");
		
		Iterator<String> it = myFriends.iterator();
		
		it.forEachRemaining(shows ->{
			System.out.println(shows);
		});
		
	}

	public static void main(String[] args) {
		forLoop();
		forEachLoop();
		iteratorWhileLoop();
		listIteratorwhileLoop();
		lambdaNForEachLoop();
		lambdaNForEachRemainingLoop();
	}

}
