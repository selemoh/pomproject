package javaInterviewQA;

public class RemoveJunkCharFromString1 {

	public static void main(String[] args) {
		
	// Use of Regular Expression to remove Junk or Unwanted Characters.	
		
		String s = "!@!#$#!#@$#Selenium Test&*()&(&)( Practice)*)(*)*";
		
		String t = "Needed &%&^$&$&$&Test%%&^& Data";
		
		String u = "Testing &$^&&$of*^*^&*^*&^ Selenium^*&^&*^*^* Scripts";
		
		String v = "Using %$^%$^$$% Java^*(&(&(&(* Language";
		
		String w = "(&(*&(Waiting for*&*&((7 Execution&66887 Ready86882923 Scripts";

		s = s.replaceAll("[^a-z A-Z]", "");
		System.out.println(s);
		System.out.println(" ");
		
		t = t.replaceAll("[^a-z A-Z]", "");
		System.out.println(t);
		System.out.println(" ");
		
		u = u.replaceAll("[^a-z A-Z]", "");
		System.out.println(u);
		System.out.println(" ");
		
		v = v.replaceAll("[^a-z A-Z]", "");
		System.out.println(v);
		System.out.println(" ");
		
		w = w.replaceAll("[^a-z A-Z]", "");
		System.out.println(w);
	}

}
