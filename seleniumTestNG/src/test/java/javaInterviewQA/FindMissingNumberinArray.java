package javaInterviewQA;

public class FindMissingNumberinArray {

	public static void main(String[] args) {

		// 1. Finding the missing number in array

		int a[] = { 1, 2, 3, 5, 6 };

		int sum = 0;

		// At first we will add the values of array. All the values should be in
		// sequence or in sorted order.

		for (int i = 0; i < a.length; i++) {
			sum = sum + a[i];
		}
		System.out.println(
				"1. -----------------------------------------------------------------------------------------------------------------------------");
		System.out.println(" ");
		System.out.println("The addition of array values is -- " + sum);
		System.out.println(" ");

		// Another for loop to add the actual sequence and get the missing number

		int sum1 = 0;

		for (int j = 1; j <= 6; j++) {
			sum1 = sum1 + j;
		}

		System.out.println("The addition of logical / sequential 'for' loop values is -- " + sum1);

		System.out.println(" ");
		System.out.println("1. The missing value in array is::::::::::::::  '" + (sum1 - sum) + "'");
		

		// 2. Finding the missing number in array

		int b[] = { 4, 5, 6, 7, 8, 9 };

		int sum2 = 0;

		// At first we will add the values of array. All the values should be in
		// sequence or in sorted order.

		for (int k = 0; k < b.length; k++) {
			sum2 = sum2 + b[k];
		}
		System.out.println(" ");
		System.out.println("2. -----------------------------------------------------------------------------------------------------------------------------");
		System.out.println(" ");
		System.out.println("The addition of array values is -- " + sum2);
		System.out.println(" ");

		// Another for loop to add the actual sequence and get the missing number

		int sum3 = 0;

		for (int l = 3; l <= 9; l++) {
			sum3 = sum3 + l;
		}

		System.out.println("The addition of logical / sequential 'for' loop values is -- " + sum3);

		System.out.println(" ");
		System.out.println("1. The missing value in array is::::::::::::::  '" + (sum3 - sum2) + "'");

	}

}
