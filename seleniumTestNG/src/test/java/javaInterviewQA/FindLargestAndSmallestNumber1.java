package javaInterviewQA;

import java.util.Arrays;

public class FindLargestAndSmallestNumber1 {

	public static void main(String[] args) {
		
		
		int num [] = {-10, 28, 50, -88, 5664, 25, 8979, -90};

		int largest = num[0];
		int smallest = num[0];
		
		for (int i = 1; i<num.length; i++) {
			if(num[i]>largest) {
				largest = num[i];
			}
			else if(num[i]<smallest) {
				smallest = num[i];
			}
		}
		
		System.out.println("\n Given array is: "+Arrays.toString(num));
		System.out.println("\n The Largest number is ::::::::: "+largest);
		System.out.println("\n The Smallest number ::::::::: "+smallest);
	}

}
