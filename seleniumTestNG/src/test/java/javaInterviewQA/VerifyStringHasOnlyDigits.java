package javaInterviewQA;

public class VerifyStringHasOnlyDigits {
	
	
	public static boolean isEmpty(CharSequence cs) {
		
		return cs == null || cs.length()==0;
	}
	
	public static boolean isNumeric(CharSequence cs) {
		if(isEmpty(cs)) {
			return false;
		}
		int len = cs.length();
		for (int i=0; i<len; i++) {
			if (! Character.isDigit(cs.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	

	public static void main(String[] args) {
		System.out.println(isNumeric(null));
		System.out.println(" ");
		System.out.println(isNumeric(" "));
		System.out.println(" ");
		System.out.println(isNumeric("Test"));
		System.out.println(" ");
		System.out.println(isNumeric("12345"));
		System.out.println(" ");
		System.out.println(isNumeric("-4343"));
		System.out.println(" ");
		System.out.println(isNumeric("123st"));
		System.out.println(" ");
		System.out.println(isNumeric("tiger567"));
		System.out.println(" ");
		System.out.println(isNumeric("//u005Cu0030"));
	}

}
