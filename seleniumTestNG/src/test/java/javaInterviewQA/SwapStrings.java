package javaInterviewQA;

public class SwapStrings {
	
	public static void main(String [] args) {
		
		System.out.println("\n===============================String swapping===============================================");
    	String c= "Hello"; //length is 5(5 letters starting from index 0)
    	String d = "World"; // length is 5(5 letters starting from index 0)
    	
    	System.out.println("\nThe value of c before swapping : "+c);
    	System.out.println("\nThe value of d before swapping : "+d);
    	
    	System.out.println("\n+++---------------------++++++++++++++++++---------------------------+++++++++++++++++------------------");
    	// Appended the strings
    	c = c + d; // HelloWorld 9(10 letters starting from index 0)
    	d = c.substring(0, (c.length()-d.length())); // HelloWorld(10) - World(5) = 5 - Substring is - Hello(index 0 to 4 - i.e 5 letters)  
    	c = c.substring(d.length()); // Substring is World (index will begin from 5 as the length of d is 5 letters)
    	
    	System.out.println("\nThe value of c after swapping : "+c);
    	System.out.println("\nThe value of d after swapping : "+d);
		
		
	}

}
