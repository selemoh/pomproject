package javaInterviewQA;

public class Reversing_An_Integer1 {

	public static void main(String[] args) {
		
	//	1. Reversing a String using own algorithm
		
		long num1 = 12345;
		long num2 = 0;
		
		while(num1 !=0) {
			num2  = num2 * 10 + num1 % 10;
			num1 = num1 / 10;
		}
		
		System.out.println(num2);
	
		System.out.println(" ");

	// 2. Reversing a String using StringBuffer
		long num3 = 12345;
		System.out.println(new StringBuffer(String.valueOf(num3)).reverse());
		
	// 3.
	System.out.println(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
		int td = 12345678;
		
		int gh = 0;
		
		while(td !=0) {
			gh = gh * 10 + td % 10;
			td = td / 10;
		}
		System.out.println(" ");
		System.out.println(gh);
		System.out.println(" ");
		
		int hhg = 987654;
		
		StringBuffer gd = new StringBuffer(String.valueOf(hhg)).reverse();
		System.out.println(gd);
		System.out.println(" ");
		
	// 4.
		System.out.println(" "); 
		
		System.out.println(" +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ");
		System.out.println(" ");
		
		int trt = 234567; 
		
		int sis = 0;
		
		while(trt != 0) {
			sis = sis * 10 + trt % 10;
			trt = trt / 10;
		}
		
		System.out.println(sis);
		
		System.out.println(" ");
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
		int ggg = 456789;
		
		StringBuffer bdd = new StringBuffer(String.valueOf(ggg)).reverse();
		
		System.out.println(bdd);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	}
}
