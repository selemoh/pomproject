package javaInterviewQA;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class FindDuplicateValueInArray {

	public static void main(String[] args) {

		System.out.println("--------------------------------  Using for loop  -------------------------------------");
		System.out.println(" ");
		String tr[] = { "Ankit", "Ashish", "Sudhanshu", "Tom", "Ankit", "Jack", "John", "Ashish", "Sudhanshu", "Tom" };

		// 1. Comparing Each element to find the duplicate value(s)
		// Time Complexity of the method is O(nxn) - It is considered as worst method

		for (int i = 0; i < tr.length; i++) {
			for (int j = i + 1; j < tr.length; j++) {
				if (tr[i].equals(tr[j])) {
					System.out.println("The duplicate value in array is  --  '" + tr[i] + "'");
				}
			}
		}
		System.out.println(" ");
		System.out.println("--------------------------------  Using HashSet  -------------------------------------");
		System.out.println(" ");
		
		
		// 2. Using HashSet to get the duplicate value(s)
		// HashSet is part of Java collection which stores unique values.
		// Time Complexity of this solution is O(n) as we are single for loop in this method.
		
		Set<String> store  = new HashSet<String>();
		for (String name : tr) {
			if(store.add(name)==false) {
				System.out.println("The duplicate value in array is  --  '" + name + "'");;
			}
		}
		
		
		System.out.println(" ");
		System.out.println("--------------------------------  Using HashMap  -------------------------------------");
	// 3. Using HashMap to get the duplicate value
	// Here in HashMap duplicate values are allowed
	// With HashMap we will be applying the logic on the occurance of the value	
	// Time Complexity of this solution is O(2n).	
		
		System.out.println(" ");
		
		Map<String, Integer> storeMap = new HashMap<String, Integer>();
		for (String name1 : tr) {
			Integer count = storeMap.get(name1);
			if (count == null) {
				storeMap.put(name1, 1);
			} else {
				storeMap.put(name1, ++count);
			}
		}

		Set<Entry<String, Integer>> dfg = storeMap.entrySet();
		for (Entry<String, Integer> entry : dfg) {
			if (entry.getValue() > 1) {
				System.out.println("The duplicate entry is -- '" + entry.getKey() + "'");
			}
		}
	}
}
